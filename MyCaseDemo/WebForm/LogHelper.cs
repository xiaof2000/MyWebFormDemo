﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;

namespace WebForm
{
    /// <summary>
    /// 日志记录类，用于记录程序运行过程中的错误信息
    /// </summary>
    public class LogHelper
    {
        //private static ILog _log;
        //private static LogHelper _logHelper = null;
        //private LogHelper(string configPath)
        //{
        //    if (!string.IsNullOrEmpty(configPath))
        //    {
        //        _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //        log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(configPath));
        //    }
        //    else
        //    {
        //        _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //    }
        //}
        //public static ILog GetInstance(string configPath)
        //{
        //    _logHelper = new LogHelper(configPath);
        //    return _log;
        //}

        /// <summary>
        /// 写错误日志
        /// </summary>
        public static void WriteError(string message)
        {
            log4net.ILog log = null;
            log = log4net.LogManager.GetLogger("SystemLog");
            log.Error(message);
        }
        public static void WriteEmailInfo(string message)
        {
            log4net.ILog log = null;
            log = log4net.LogManager.GetLogger("EMailLog");
            log.Info(message);
        }
    }
}