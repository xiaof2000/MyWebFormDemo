﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ValidateInputMore.aspx.cs" Inherits="WebForm.ValidateInputMore" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
        <style type="text/css">
        .auto-style2 {
            width: 406px;
        }

        .auto-style3 {
            width: 215px;
        }


        label.error {
            color: Red;
            font-size: 13px;
            margin-left: 5px;
            padding-left: 16px;
        }
    </style>
    <script src="Scripts/jquery-1.8.2.js"></script>
    <script src="Scripts/jquery.validate.js"></script>
    <script src="Scripts/jquery.validate_zh.js"></script>
</head>
<body>
    <h1>多个form的输入验证</h1>
    <div >1、如果是用webform的验证控件，可以设置组名和区分验证组，并在按钮中设置验证的组名</div>
    <div >2、如果是用jQuery 的插件。。。正在测试。。。</div>

    
    <form id="form1" runat="server">
        <div>
    <h3>第1组表单验证</h3>    
         <table style="width: 98%">
                <tr>

                    <td style="text-align: right" class="auto-style3">*姓名：</td>
                    <td class="auto-style2">
                        <asp:TextBox ID="txtOrderingManName" MaxLength="30" runat="server" Width="180px" placeholder="请输入您的姓名"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="第1组" ValidationGroup="a" ControlToValidate="txtOrderingManName"></asp:RequiredFieldValidator>
                    </td>
                    <td>&nbsp;</td>
                </tr>
              
                <tr>
                    <td style="text-align: right" class="auto-style3">*留言：</td>
                    <td class="auto-style2">
                        <asp:TextBox ID="txtTheMessage" MaxLength="200" runat="server" Width="500px" Height="68px" TextMode="MultiLine" placeholder="请输入您所需要的产品数据、质量要求等"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="第1组" ValidationGroup="a" ControlToValidate="txtTheMessage"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">

                        <asp:Label ID="lblMsg" runat="server" BackColor="Transparent" ForeColor="Red"></asp:Label>
                        <asp:Button ID="btnSumbit" runat="server" Text="提 交" OnClick="btnSumbit_Click"  />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    
    <hr />
    <div style="background-color:cadetblue">
    <h3>第2组表单验证(如果用asp.net的验证控件时，设置此“查询”按钮的CausesValidation属性为Flase，就不会进行验证。或：也可以在控件上加上验证分组，进行分组验证）</h3>
    
        <div >查询：                   
            <asp:TextBox ID="txtFind" MaxLength="30" runat="server" Width="180px" placeholder="请输入查询条件"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFind" ErrorMessage="第2组" ValidationGroup="b"></asp:RequiredFieldValidator>
        </div>
   
    
        <asp:Button ID="Button1" runat="server" Text="查询" OnClick="Button1_Click" ValidationGroup="b" CausesValidation="False" />

    </div>

            <hr />
    <div style="background-color:azure">
    <h3>第3组表单验证(如果用jQuery插件验证时，设置此“查询”按钮的CssClass为cancel时，jQuery Validate插件就不会进行验证）</h3>
    
        <div >查询：                   
            <asp:TextBox ID="txtSearch" MaxLength="30" runat="server" Width="180px" placeholder="请输入查询条件"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtFind" ErrorMessage="第2组" ValidationGroup="b"></asp:RequiredFieldValidator>
        </div>
   
    
        <asp:Button ID="btnSearch" runat="server" Text="查询" CssClass="cancel" OnClick="btnSearch_Click" />

    </div>


     </form>
    <script type="text/javascript">
        $(function () {
            $("#form1").validate({
                rules: {
                    txtOrderingManName: {
                        required: true,
                        minlength: 2
                    },
                    txtTheMessage: {
                        required: true,
                        minlength: 5,
                        maxlength: 200
                    },

                    txtSearch: {
                                        required: true,
                        
                                    },
                },
            });

        //    $("#form3").validate({
        //        rules: {
        //            txtFind: {
        //                required: true,
        //                minlength: 2
        //            },
        //        },

        //    });

        });

    </script>
</body>
</html>