﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadImageAndCut.aspx.cs" Inherits="WebForm.UploadImageAndCut" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <title>图片上传并截图</title>
    <link href="Scripts/uploadify/uploadify.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.8.2.min.js"></script>
    <script src="Scripts/plugin_layer/layer.min.js"></script>
    <script src="Scripts/uploadify/jquery.uploadify.min.js"></script>
    <script type="text/javascript">
        $(function () {
            var picName;//图片名称
            function cutImage () {
                var sj = Math.random();

                $.layer({
                    type: 2,
                    maxmin: true,
                    title: '图片裁剪',
                    shadeClose: true, //开启点击遮罩关闭层
                    area: ['700px', '550px'],
                    offset: ['100px', ''],
                    iframe: { src: 'ImageCutNoPreview.aspx?newWidth=200&newHeight=100&picName=' + picName }
                });
            };

            $("#uploadify").uploadify({
                //指定swf文件
                'swf': 'Scripts/uploadify/uploadify.swf',
                //后台处理的页面
                'uploader': 'UploadHandler.ashx',
                //按钮显示的文字
                'buttonText': '上传图片',
                //显示的高度和宽度，默认 height 30；width 120
                //'height': 15,
                //'width': 80,
                //上传文件的类型  默认为所有文件    'All Files'  ;  '*.*'
                //在浏览窗口底部的文件类型下拉菜单中显示的文本
                'fileTypeDesc': 'Image Files',
                //允许上传的文件后缀
                'fileTypeExts': '*.gif; *.jpg; *.png',
                //发送给后台的其他参数通过formData指定
                //'formData': { 'someKey': 'someValue', 'someOtherKey': 1 },
                //上传文件页面中，你想要用来作为文件队列的元素的id, 默认为false  自动生成,  不带#
                //'queueID': 'fileQueue',
                //选择文件后自动上传
                'auto': true,
                //设置为true将允许多文件上传
                'multi': true,
                'removeCompleted':false,
                'onUploadSuccess': function (file, data, response) {
                    picName = file.name;
                    alert('上传完成，图片名称为：' + picName);
                    cutImage();
                }
            });
        });


    </script>
</head>
<body>
    <form id="form1" runat="server">
 <div>
 

        <%--用来作为文件队列区域--%>
        <div id="fileQueue">
        </div>
        <input type="file" name="uploadify" id="uploadify" />
        <p>
            <a href="javascript:$('#uploadify').uploadify('upload')">上传</a>| 
            <a href="javascript:$('#uploadify').uploadify('cancel')">取消上传</a>
       
        </p>
    </div>

    
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>

    <h2>图片上传并截图（一个弹出窗口版）</h2>
    <a id="UploadAndCut" href="#">只指定图片大小</a><br />
    <a id="UploadAndCut1" href="#">指定图片大小并指定图片名称 参数是图片1</a>
    <a id="UploadAndCut2" href="#">指定图片大小并指定图片名称 参数是图片2</a>
    <script type="text/javascript">

        $("#UploadAndCut").click(function () {
            $.layer({
                type: 2,
                maxmin: true,
                scrollbar: false,
                title: '图片上传并裁剪',
                shadeClose: true, //开启点击遮罩关闭层
                area: ['700px', '550px'],
                offset: ['100px', ''],
                iframe: { src: 'UploadAndCut.aspx?returnControlId=hfdFilename&newWidth=200&newHeight=100' }
            });
        });

        $("#UploadAndCut1").click(function () {
            $.layer({
                type: 2,
                maxmin: true,
                scrollbar: false,
                title: '图片上传并裁剪',
                shadeClose: false, //开启点击遮罩关闭层
                area: ['700px', '550px'],
                offset: ['100px', ''],
                iframe: { src: 'UploadAndCut.aspx?returnControlId=<%= hfdFileName.ClientID %>&newWidth=200&newHeight=100&picName=/abc/123/1.jpg' }

            });
        });

        $("#UploadAndCut2").click(function () {
            $.layer({
                type: 2,
                maxmin: true,
                scrollbar: false,
                title: '图片上传并裁剪',
                shadeClose: false, //开启点击遮罩关闭层
                area: ['700px', '550px'],
                offset: ['100px', ''],
                iframe: { src: 'UploadAndCut.aspx?returnControlId=<%= hfdFileName.ClientID %>&newWidth=200&newHeight=200&folder=aaabbb/dd' },
                end: function () {
                    alert('窗口关闭');
                }
                

                
                
            });
        });

        ////打开图片上传裁剪弹出层
        //function openImageUploadCutBox(绑定的控件Id, 绑定的返回值控件Id，图片宽，图片高，上传目录，上传文件名 )
        //{ }
        

        function openCutImage() {
            var l = $("#imgName").attr('src');

            $.layer({
                type: 2,
                scrolling: false,
                //title: '图片上传裁剪',
                shadeClose: false, //开启点击遮罩关闭层
                area: ['700px', '600px'],
                offset: ['100px', ''],
                iframe: { src: 'UploadAndCut.aspx?returnControlId=<%= hfdFileName.ClientID %>&newWidth=400&newHeight=300&folder=UploadFiles&picName=' + l },
                end: function () {
                    var r = $("#<%= hfdFileName.ClientID %>").val();
                    //var m = r + '?number=' + Math.random();
                    var m = r + '?time=' + new Date().getTime();
                    if (r.length > 0) {
                        $("#imgName").attr('src', m);
                        $("#imgurllink").attr('href', r);
                    }
                }
            });
        }

    </script>
        <p>
            &nbsp;</p>
        
           <h2> 以下为比较完整功能的演示</h2>
        <p>
            <a id="imgurllink" href="<%= imgurl %>" target="_blank">
                <img id="imgName" border="0" onload="DrawImage(this,140);" src="<%=imgurl %>" />
            </a>
            <input type="button" id="btnUpload" onclick="openCutImage();" value="【上传图片】" />
            <asp:HiddenField ID="hfdFileName" runat="server" />
        </p>
        <p>
            <asp:Button ID="btnTest" runat="server" Text="Button" OnClick="btnTest_Click" />
        </p>
    </form>

    
    </body>
</html>
