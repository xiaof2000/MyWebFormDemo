﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForm
{
    /// <summary>
    /// 动态类型dynamic的使用
    /// </summary>
    public partial class DynamicTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Dictionary<int, dynamic>  o = GetRatingGroup(1);
            Response.Write( o[1].rating + "-" + o[1].num + "-ok");
        }

        public Dictionary<int, object> GetRatingGroup(int shopId)
        {
            //获取每个级别的数据
            //where shopId= shopId

            var o = new { rating = 1, num = 2, sumRating = 3 };
            Dictionary<int, object> d = new Dictionary<int, object>();
            d.Add(1, o);
            return d;
        }
    }
}