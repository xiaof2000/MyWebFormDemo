﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Demo.aspx.cs" Inherits="WebForm.Webuploader.Demo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <a href="UploadCutImage.aspx">上传并裁图</a>
    </div>

         <script  type="text/javascript">
             function DrawImage(ImgD, maxSize) {
                 //debugger;
                 var image = new Image();
                 image.src = ImgD.src;
                 if (image.width > 0 && image.height > 0) {
                     if (image.width / image.height >= 1) {
                         if (image.width > maxSize) {
                             ImgD.width = maxSize;
                             ImgD.height = (image.height * maxSize) / image.width;
                         }
                         else {
                             ImgD.width = image.width;
                             ImgD.height = image.height;
                         }
                     }
                     else {
                         if (image.height > maxSize) {
                             ImgD.height = maxSize;
                             ImgD.width = (image.width * maxSize) / image.height;
                         }
                         else {
                             ImgD.width = image.width;
                             ImgD.height = image.height;
                         }
                     }
                 }
             }
    </script>
    <div class="user_right_box">
        <div class="user_right_top_tit_bg">
            <h1>修改形象照片</h1>
        </div>
        <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
                <td height="50" colspan="2" bgcolor="#fafafa" style="border-bottom: 1px #CCCCCC dashed; color: #F60; text-align: left; padding-left: 20px;">
                    <span style="font-size: 14px; line-height: 30px;">提示：修改门头照将花费箕子豆<asp:Label ID="lbuseBeans" runat="server"></asp:Label>个
                    </span>
                </td>
            </tr>
            <tr>
                <td align="right">LOGO：
                </td>
                <td width="*" align="left">
                    <a id="imgurllink" href ="<%= imgurl %>" target="_blank">
                         <img id="imgName" border="0" onload="DrawImage(this,300);" src="<%=imgurl %>" alt="查看原图"/>
                    </a>
                    <asp:HiddenField ID="lblShopId" runat="server" EnableViewState="False" />
                    <br />
                    <asp:HiddenField ID="hfdFileName" runat="server"  />
                    <br />
                    <strong style="color: Red">建议图片大小：400*300像素，图片名称中不能包含中文字符！<strong style="color: Red">可上传*.jpg、*.gif、*.png、*.bmp 格式</strong></strong>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="right"></td>
                <td align="left" width="*">
                    <input type="button" id="btnUpload" onclick="openCutImage();" value="上传图片" class="but100" />&nbsp;&nbsp;&nbsp;
                   </td>
            </tr>
        </table>
        <p style="text-align: center">
            &nbsp;
        </p>
    </div>
    <%--打开图片上传裁剪窗口--%>
    <script src="../Scripts/jquery-1.8.2.js" type="text/javascript"></script>
    <script src="../Scripts/plugin_layer/layer.min.js" type="text/javascript"></script>
    </form>

     <script type="text/javascript">
         function openCutImage() {
             var l = $("#imgName").attr('src');

             $.layer({
                 type: 2,
                 maxmin: true,

                 title: '图片上传裁剪',
                 shadeClose: false, //开启点击遮罩关闭层
                 area: ['650px', '520px'],
                 offset: ['50px', ''],
                 iframe: { src: 'UploadCutImage.aspx?returnControlId=<%= hfdFileName.ClientID %>&newWidth=400&newHeight=300&picName=' + l },
                 //iframe: { src: 'UploadCutImage.aspx?returnControlId=<%= hfdFileName.ClientID %>&newWidth=400&newHeight=300&folder=shoplogo&picName=' + l },
                end: function () {
                    var r = $("#<%= hfdFileName.ClientID %>").val();
                    var m = r + '?time=' + new Date().getTime();
                    if (r.length > 0) {
                        $("#imgName").attr('src', m);
                        $("#imgurllink").attr('href', r);
                    }
                }
            });
        }
    </script>
</body>
</html>
