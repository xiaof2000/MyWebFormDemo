﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadCutImage.aspx.cs" Inherits="WebForm.Webuploader.UploadCutImage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>文件上传</title>
    <link href="/Scripts/webuploader/webuploader.css" rel="stylesheet" />
     <%--截图用css,js--%>
    <link href="/Scripts/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet" />

    <style type="text/css">
        #toolBar {
            height: 50px;
            width: 100%;
            position: fixed;
            bottom: 0;
            z-index: 900;
        }

        #toolBarPanel {
            float: right;
            width: 260px;
            line-height: 50px;
        }

        .image-box {
            min-width: 580px;
            min-height: 400px;
        }

        /*上传队列样式*/
        .displayUpload {
            display: block;
            top: 30%;
            left: 30%;
            position: absolute;
            width: 300px;
            z-index: 800;
        }
    </style>
    <script type="text/javascript">
        //关闭弹出层
        function closeBox() {
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
        }

        //返回值
        function returnValue(returnControlId,filename)
        {
            var obj= window.parent.document.getElementById(returnControlId);
            if (obj!=null) {
                obj.value=filename;
                closeBox();
            }
        }
    </script>
    
</head>
<body>
    <form id="form1" runat="server">
        <div id="uploader" class="wu-example">
            <!--用来存放文件信息-->
            <div class="btns">
                <span id="picker">选择文件</span>&nbsp;&nbsp;
                <span id="thelist" class="uploader-list" style="line-height: 38px;display: block;float: right;text-align: left;width: 570px; clear:both;"></span>    
            </div>
            
        </div>
         <div class="container">
            <div class="image-box">
                <asp:Image ID="target" runat="server" alt="图片" />
            </div>
        </div>
           <asp:HiddenField ID="x1" runat="server" />
        <asp:HiddenField ID="y1" runat="server" />
        <asp:HiddenField ID="x2" runat="server" />
        <asp:HiddenField ID="y2" runat="server" />
        <asp:HiddenField ID="Iwidth" runat="server" />
        <asp:HiddenField ID="Iheight" runat="server" />
        <asp:HiddenField ID="imgFileName" runat="server" />
        <asp:HiddenField ID="hfdPrivateFolder" runat="server" />
        <asp:HiddenField ID="imgFilePath" runat="server" />
        

        <div id="toolBar">
            <div id="toolBarPanel">
                <asp:Button ID="btnSaveImage" runat="server" Text="确定" OnClick="saveImg" Width="65px" />
                &nbsp;&nbsp;
                <input id="btnClose" type="button" value="取消" style="width: 65px" onclick="closeBox();" />

            </div>
        </div>
    </form>
    
    <script src="/Scripts/jquery-1.8.2.js"></script>
    <script src="/Scripts/webuploader/webuploader.js"></script>
    <script src="/Scripts/jcrop/js/jquery.Jcrop.min.js" type="text/javascript"></script>

    <script type="text/javascript">
    
        var jcrop_api,
               boundx,
               boundy,

               // Grab some information about the preview pane
               $preview = $('#image-box'),

              // xsize = $pcnt.width(),
               //ysize = $pcnt.height();
               xsize = <%= _sizeWidth %>,
                ysize = <%= _sizeHeight %>;


        //console.log('init', [xsize, ysize]);//输出宽高数值

        $(function () {

            
            //重新显示一个要裁剪的图片
            function showImage(folder,privateFolder,picName) {
                jcrop_api.setImage( folder + privateFolder + picName);
            }

            //$("#cutImage").click(function(){
            $('#target').Jcrop({
                //setSelect: [ 0, 0, 540, 330 ],//默认加上选择框
                boxWidth:630, //设置宽度
                boxHeight:460,
                onChange: updatePreview,
                onSelect: updatePreview,
                aspectRatio: xsize / ysize
            }, function () {
                // Use the API to get the real image size
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];
                // Store the API in the jcrop_api variable
                jcrop_api = this;

                // Move the preview into the jcrop container for css positioning
                $preview.appendTo(jcrop_api.ui.holder);
            });


            function updatePreview(c) {
                if (parseInt(c.w) > 0) {
                    $('#x1').val(c.x);
                    $('#y1').val(c.y);
                    $('#x2').val(c.x2);
                    $('#y2').val(c.y2);
                    $('#Iwidth').val(c.w);
                    $('#Iheight').val(c.h);
                };
            };
        });        
   

        //图片上传******

        //上传初始化
        var uploader = WebUploader.create({

            // swf文件路径
            swf: '/Scripts/webuploader/Uploader.swf',

            // 文件接收服务端。
            server: '/Scripts/webuploader/fileupload.ashx?folder=' + $('#imgFilePath').val() + '&privateFolder=' + $('#hfdPrivateFolder').val(),

            auto: true,//自动上传
            //只允许选择图片
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            },

            fileSizeLimit: 1 * 1024 * 1024,    // 1 M 上传的所有文件大小限制
            fileSingleSizeLimit: 1 * 1024 * 1024,    // 1 M 上传的个人文件大小限制

            // 选择文件的按钮。可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: '#picker',

            // 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
            resize: false
        });

        $list = $('#thelist');

        //加入出错时的提示
        uploader.on('error', function (handler) {
            if (handler == "Q_EXCEED_NUM_LIMIT") {
                alert("超出最大张数");
            }
            if (handler == "Q_EXCEED_SIZE_LIMIT") {
                alert("选择的文件超过了文件大小1M的限制，无法上传！");
            }
        });

        uploader.on('uploadError', function (file) {
            $list.text("上传时出错！");
        });


        //文件上传失败会派送uploadError事件，成功则派送uploadSuccess事件。不管成功或者失败，在文件上传完后都会触发uploadComplete事件。
        uploader.on('uploadSuccess', function (file, response) {
            
            //var fname=response._raw
            var fname=response.newName;
            $list.text("上传成功！");
            $list.text("上传成功！新文件名：" + fname);
            //alert('文件已上传，原文件名：' + file.name + ' 新文件名:' + fname);
            //上传成功后显示图片
            $("#imgFileName").val(fname);
            var fullName= $('#imgFilePath').val()+ $('#hfdPrivateFolder').val() +fname;
            jcrop_api.setImage(fullName);
        });

    </script>
</body>
</html>
