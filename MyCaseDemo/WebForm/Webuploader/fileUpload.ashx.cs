﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebForm.Webuploader
{
    /// <summary>
    /// fileUpload 的摘要说明
    /// </summary>
    public class fileUpload : IHttpHandler
    {
        private string _uploadPath = "UploadFiles";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Request.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");
            context.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");
            context.Response.Charset = "utf-8";

            //接收上传后的文件
            HttpPostedFile file = context.Request.Files["file"];

            //context.Response.Write("Hello World");

            string filePathName = string.Empty;

            string localPath = Path.Combine(HttpRuntime.AppDomainAppPath, _uploadPath);
            if (context.Request.Files.Count == 0)
            {
                //return Json(new { jsonrpc = 2.0, error = new { code = 102, message = "保存失败" }, id = "id" });
                context.Response.Write("0");
            }

            string ex = Path.GetExtension(file.FileName);
            filePathName = Guid.NewGuid().ToString("N") + ex;
            if (!System.IO.Directory.Exists(localPath))
            {
                System.IO.Directory.CreateDirectory(localPath);
            }
            file.SaveAs(Path.Combine(localPath, filePathName));
            
            string r="/" + _uploadPath + "/" + filePathName;

            //json = string.Format("{{ \"fileFullName\":{0} }}", 1);   //返回json数据 测试通过
            //json = string.Format("{{\"fileFullName\":{0}}}", "10");  //返回json数据 测试通过
            //json = string.Format("{{\"fileFullName\":\"{0}\"}}", "abc");  //返回json数据 测试通过
            //json = string.Format("{{\"fileFullName\":\"{0}\",\"filePath\":\"{1}\" }}", "abc","df");  //返回json数据 测试通过
            //json = string.Format("{{\"fileFullName\":\"{0}\",\"id\":{1} }}", "abc", 1);              //返回json数据 测试通过
            //json = "{{\"fileFullName\":\"df\"}}"; //返回json，测试不行！！

            r = "e:\\abc\\df\\1.jpg";
            string json = string.Format("{{\"id\":{0},\"newName\":\"{1}\",\"fileFullName\":\"{2}\" }}",1,filePathName, r);  //返回json数据 测试通过
            context.Response.Write(json);

            
            //返回json后，可在前台用以下方法获取json对象的属性值
            //uploader.on('uploadSuccess', function (file, response) {
            ////response对象为返回的json对象
            //var fname = response.fileFullName;
            //$list.text("上传成功！");
            //$list.text("上传成功！新文件名：" + fname);
            //alert('文件已上传，原文件名：' + file.name + ' 新文件名:' + response._raw);
            //});

            

            //return Json(new
            //{
            //    jsonrpc = "2.0",
            //    id = id,
            //    filePath = "/Upload/" + filePathName
            //});

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}