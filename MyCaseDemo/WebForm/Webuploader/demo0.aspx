﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="demo0.aspx.cs" Inherits="WebForm.Webuploader.demo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>文件上传</title>

    <link href="../Scripts/webuploader/webuploader.css" rel="stylesheet" />
    <%--<link href="../Content/upload.css" rel="stylesheet" />--%>
    <script src="../Scripts/jquery-1.8.2.min.js"></script>
    <script src="../Scripts/webuploader/webuploader.min.js"></script>

    <style type="text/css">
        .uploadArea {
            margin: 0 auto;
            width: 480px;
            height: 400px;
            background-color: #eef1f6;
            border: dashed 1px #808080;
            padding: 20px;
            position:relative;
        }

        .fileDisplay {
            /*margin: 20px auto auto auto;*/
            width: 480px;
            height: 360px;
            position: relative;
            background: #e9e9db; /*#eef1f6*/
            z-index: 95;
            top: 0px;
            left: 0px;
        }

        .toolbarPanel {
            position: absolute;
            height: 180px;
            width: 480px;
            text-align: center;
            font-size: 18px;
            z-index: 100;
            /*clear: both;*/
            padding-top: 180px;
            top: 20px;
        }

        /*上传成功样式*/

        .state-done {
            background: url(../images/success.png) no-repeat;
            position: absolute;
            line-height: 40px;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            bottom: 0px;
            right: 0px;
            width: 40px;
            height: 40px;
            color: #4cae4c;
            z-index: 99;
        }

        /*上传进度条*/
        .fileDisplay .progress {
            position: absolute;
            right: 4px;
            bottom: 4px;
            height: 3px;
            left: 4px;
            height: 4px;
            overflow: hidden;
            z-index: 15;
            margin: 0;
            padding: 0;
            border-radius: 0;
            background: transparent;
        }

        .fileDisplay .progress span {
            display: block;
            overflow: hidden;
            width: 0;
            height: 100%;
            background: #d14 url(../Images/progress.png) repeat-x;
            -webit-transition: width 200ms linear;
            -moz-transition: width 200ms linear;
            -o-transition: width 200ms linear;
            -ms-transition: width 200ms linear;
            transition: width 200ms linear;
            -webkit-animation: progressmove 2s linear infinite;
            -moz-animation: progressmove 2s linear infinite;
            -o-animation: progressmove 2s linear infinite;
            -ms-animation: progressmove 2s linear infinite;
            animation: progressmove 2s linear infinite;
            -webkit-transform: translateZ(0);
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="uploader" class="uploadArea">
            <div id="fileList" class="fileDisplay">
                <div id="showState"></div>
                <asp:Image ID="imgUpload" runat="server"  />
            </div>
            <div id="filePicker" class="toolbarPanel"></div>
            <asp:HiddenField ID="hfdFileName" runat="server" />
        </div>
        选择文件：<asp:Label ID="lblFileName" runat="server" Text="Label"></asp:Label>

    </form>

    <script type="text/javascript">
        $(function () {
            var $ = jQuery,
            $list = $('#fileList'),
            // 优化retina, 在retina下这个值是2
            ratio = window.devicePixelRatio || 1,
            // 缩略图大小
            thumbnailWidth = 480 * ratio,
            thumbnailHeight = 360 * ratio,
            uploader;// Web Uploader实例

            if (!WebUploader.Uploader.support()) {
                alert('Web Uploader 不支持您的浏览器！如果你使用的是IE浏览器，请尝试升级 flash 播放器');
                throw new Error('WebUploader does not support the browser you are using.');
            }


            uploader = WebUploader.create({
                swf: '../../Scripts/webuploader/Uploader.swf',// swf文件路径
                server: '../../Scripts/webuploader/fileUpload.ashx',// 文件接收服务端
                auto: true,// 选完文件后，是否自动上传。
                resize: false,// 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
                duplicate: true,//[可选] [默认值：undefined] 去重， 根据文件名字、文件大小和最后修改时间来生成hash Key.
                fileSizeLimit: 20 * 1024 * 1024,    // 1 M 上传的所有文件大小限制
                fileSingleSizeLimit: 20 * 1024 * 1024,    // 1 M 上传的个人文件大小限制 


                // 选择文件的按钮。可选。
                // 内部根据当前运行是创建，可能是input元素，也可能是flash.
                pick:
                    {
                        id: '#filePicker',
                        label: '请选择文件',
                        //label: '<span class="fileSelect">请选择文件<span>',
                        //innerHTML: '<span class="fileSelect">请选择文件<span>',
                        multiple: false
                    },


                //拖拽设置
                dnd: '#fileList',
                disableGlobalDnd: true,
                paste: document.body,

                //图片设置
                thumb: {
                    //width: 110,
                    //height: 110,
                    //quality: 70, // 图片质量，只有type为`image/jpeg`的时候才有效。

                    crop: false, // 是否允许裁剪
                    allowMagnify: true,// 是否允许放大，如果想要生成小图的时候不失真，此选项应该设置为false.

                },

                //只允许选择图片
                accept: {
                    title: 'Images',
                    extensions: 'gif,jpg,jpeg,bmp,png',
                    mimeTypes: 'image/*'
                }
            });

            // 当有文件添加进来的时候
            uploader.on('fileQueued', function (file) {
                //删除上传成功标记
                $("#showState").removeClass("state-done");

                var $img = $("#imgUpload");

                // 创建缩略图
                // 如果为非图片文件，可以不用调用此方法。
                // thumbnailWidth x thumbnailHeight 为 100 x 100
                uploader.makeThumb(file, function (error, src) {
                    if (error) {
                        $img.replaceWith('<span>不能预览</span>');
                        return;
                    }

                    $img.attr('src', src);

                }, thumbnailWidth, thumbnailHeight);

            });


            /* 错误处理 */
            //加入出错时的提示
            uploader.on('error', function (handler) {
                if (handler == "Q_EXCEED_NUM_LIMIT") {
                    alert("超出最大张数");
                }
                if (handler == "Q_EXCEED_SIZE_LIMIT") {
                    alert("选择的文件超过了文件大小1M的限制，无法上传！");
                }
            });

            // 文件上传成功，给item添加成功class, 用样式标记上传成功。
            uploader.on('uploadSuccess', function (file, response) {
                $("#imgUpload").show();
                $('#showState').addClass('state-done');
                var fname = response.newName;
                $("#lblFileName").text(fname);
                $("#<%= hfdFileName.ClientID %>").val(fname)
                $("#filePicker").hide();

            });

            // 文件上传失败，显示上传出错。
            uploader.on('uploadError', function (file) {
                //var $li = $('#' + file.id),
                //    $error = $li.find('div.error');

                //// 避免重复创建
                //if (!$error.length) {
                //    $error = $('<div class="error"></div>').appendTo($li);
                //}

                //$error.text('上传失败');

                alert("文件上传失败");
            });

            //// 完成上传完了，成功或者失败，先删除进度条。
            //uploader.on('uploadComplete', function (file) {
            //    $('#' + file.id).find('.progress').remove();
            //});

            ////所有文件上传完毕
            //uploader.on("uploadFinished", function ()
            //{
            //    //提交表单
            //    alert("上传成功");
            //});


            //// 当有文件添加进来的时候
            //uploader.on('fileQueued', function (file) {
            //    var $li = $(
            //            '<div id="' + file.id + '" class="cp_img">' +
            //                '<img>' +
            //            '<div class="cp_img_jian"></div></div>'
            //            ),
            //        $img = $li.find('img');


            //    // $list为容器jQuery实例
            //    //$list.append($li);
            //    $list.html($li);



            //// 文件上传过程中创建进度条实时显示。
            //uploader.on('uploadProgress', function (file, percentage) {
            //    var $li = $('#' + file.id),
            //        $percent = $li.find('.progress span');

            //    // 避免重复创建
            //    if (!$percent.length) {
            //        $percent = $('<p class="progress"><span></span></p>')
            //                .appendTo($li)
            //                .find('span');
            //    }

            //    $percent.css('width', percentage * 100 + '%');
            //});

            // 文件上传过程中创建进度条实时显示。
            uploader.on('uploadProgress', function (file, percentage) {
                var $li = $('#fileList'),
                    $percent = $li.find('.progress span');

                // 避免重复创建
                if (!$percent.length) {
                    $percent = $('<p class="progress"><span></span></p>')
                            .appendTo($li)
                            .find('span');
                }

                $percent.css('width', percentage * 100 + '%');
            });

            // 完成上传完了，成功或者失败，先删除进度条。
            uploader.on('uploadComplete', function (file) {
                $('#fileList').find('.progress').remove();
            });

            ////显示删除按钮
            //$(".cp_img").live("mouseover", function ()
            //{
            //    $(this).children(".cp_img_jian").css('display', 'block');

            //});
            ////隐藏删除按钮
            //$(".cp_img").live("mouseout", function () {
            //    $(this).children(".cp_img_jian").css('display', 'none');

            //});
            ////执行删除方法
            //$list.on("click", ".cp_img_jian", function ()
            //{
            //    var Id = $(this).parent().attr("id");
            //    uploader.removeFile(uploader.getFile(Id,true));
            //    $(this).parent().remove();
            //});


            //var $li = $("#fileList");
            ////上传按钮
            //$btns = $('<div class="filePanel">' +
            //    '<span class="cancel">删除</span>' +
            //    '<span class="rotateRight">向右旋转</span>' +
            //    '<span class="rotateLeft">向左旋转</span></div>').appendTo($li),

            //uploader.addButton({
            //    id: '#filePanel',
            //    label: '继续添加'
            //});
            /*$("#imgUpload").mouseover(function () {
                $("#filePicker").show();
            });
            $("#imgUpload").mouseout(function () {
                $("#filePicker").hide();
            });*/

            //显示按钮
            $("#fileList").on('mouseover', function () {
             //   $("#filePicker").stop().animate({ display: 'block' });
               $("#filePicker").show();
            });

            $("#fileList").on('mouseout', function () {
                //$("#filePicker").stop().animate({ display: none });
                if ($("#<%= hfdFileName.ClientID %>").val().length > 0)
                    {$("#filePicker").hide();}
            });

            $("#filePicker").on('mouseover', function () {
                //   $("#filePicker").stop().animate({ display: 'block' });
                $("#filePicker").show();
            });

            $("#filePicker").on('mouseout', function () {
                //$("#filePicker").stop().animate({ display: none });
                if ($("#<%= hfdFileName.ClientID %>").val().length > 0)
                    {$("#filePicker").hide();}
            });
        });
    </script>
</body>
</html>
