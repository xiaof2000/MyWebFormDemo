﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForm
{
    public partial class LayerDemo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "load...";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string s = "var index = parent.layer.getFrameIndex(window.name);" + "parent.layer.close(index);";

            this.Page.ClientScript.RegisterStartupScript(GetType(), "提示", "<script>" + s + "</script>");
        }
    }
}