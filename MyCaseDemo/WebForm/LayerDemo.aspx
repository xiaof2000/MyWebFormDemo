﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LayerDemo.aspx.cs" Inherits="WebForm.LayerDemo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>调用弹出的层</title>
    <script src="Scripts/jquery-1.8.2.min.js"></script>
    <script src="Scripts/plugin_layer/layer.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <button id="showThis" type="button" >订购产品</button>
        <div id="showThis2">订购</div>
        <div id="showThis3">订购iFrame<br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
        </div>

        <a href="javascript:window.opener=null;window.open('','_self');window.close();">关闭</a>
    </div>
    </form>
    <script type="text/javascript">
        //弹出一个页面层
        $('#showThis2').on('click', function () {
            $.layer({
                type: 1,
                title: false, //不显示默认标题栏
                shade: [0], //不显示遮罩
                area: ['600px', '360px'],
                page: { html: '<img src="http://static.oschina.net/uploads/space/2014/0516/012728_nAh8_1168184.jpg" alt="layer">' }
            });
        });

        //弹出一个iframe层
        $('#showThis3').on('click', function () {
            $.layer({
                type: 2,
                title: 'iframe父子操作',
                maxmin: true,
                shadeClose: true, //开启点击遮罩关闭层
                area: ['800px', '460px'],
                offset: ['100px', ''],
                iframe: { src: 'LayerShow.aspx' }
            });
        });
  
        //弹出一个iframe层
        $('#showThis').on('click', function () {
            $.layer({
                type: 2,
                title: 'iframe父子操作',
                maxmin: true,
                shadeClose: true, //开启点击遮罩关闭层
                area: ['800px', '460px'],
                offset: ['100px', ''],
                iframe: { src: 'LayerShow.aspx' }
            });
        });

    </script>
</body>
   
</html>
