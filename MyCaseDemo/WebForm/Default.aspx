﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebForm._Default" %>

  
<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <%--<section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %>.</h1>
                <h2>Modify this template to jump-start your ASP.NET application.</h2>
            </hgroup>
            <p>
                To learn more about ASP.NET, visit <a href="http://asp.net" title="ASP.NET Website">http://asp.net</a>.
                The page features <mark>videos, tutorials, and samples</mark> to help you get the most from ASP.NET.
                If you have any questions about ASP.NET visit
                <a href="http://forums.asp.net/18.aspx" title="ASP.NET Forum">our forums</a>.
            </p>
        </div>
    </section>--%>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <div>
        <h3>其它帮助</h3>
        <p>图片转base64工具地址 http://www.w3cmark.com/a/tools/base64.html

        </p>
    </div>
   <%-- <h3>We suggest the following:</h3>
    <ol class="round">
        <li class="one">
            <h5>Getting Started</h5>
            ASP.NET Web Forms lets you build dynamic websites using a familiar drag-and-drop, event-driven model.
            A design surface and hundreds of controls and components let you rapidly build sophisticated, powerful UI-driven sites with data access.
            <a href="http://go.microsoft.com/fwlink/?LinkId=245146">Learn more…</a>
        </li>
        <li class="two">
            <h5>Add NuGet packages and jump-start your coding</h5>
            NuGet makes it easy to install and update free libraries and tools.
            <a href="http://go.microsoft.com/fwlink/?LinkId=245147">Learn more…</a>
        </li>
        <li class="three">
            <h5>Find Web Hosting</h5>
            You can easily find a web hosting company that offers the right mix of features and price for your applications.
            <a href="http://go.microsoft.com/fwlink/?LinkId=245143">Learn more…</a>
        </li>
    </ol>--%>
    <p>
        &nbsp;</p>
    <p>
        <a href="KindeditorDemo.aspx">富文本编辑器</a></p>
    <a href="CallBackCode.aspx">前后台调用</a>
    <hr />
    <p>
        <a href="ValidateInput.aspx">jQuery验证插件1</a>&nbsp;&nbsp; 
        <a href="ValidateInput.aspx">设置某个按钮不验证</a>
    </p>
    <hr />
    <p>
        <a href="ReturnKeyTest.aspx">回车时等于点某个按钮</a>&nbsp;&nbsp; 

    </p>
    <hr />
    <p>
       <a href="gundong.aspx">滚动图片</a> （ jCarousel插件老版 ）</p>
    <p>
       <a href="gundongNew.aspx">滚动图片</a> （ jCarousel插件新版 ）</p>
    <hr />
    <a href="ZTreeTest.aspx">zTree 插件测试</a>
    <hr />
    <a href="SendEmail.aspx">发邮件</a>
    <br />
    <a href="SendSMS.aspx">发短信</a>
    <hr />
    <h3>以下用的上传插件对浏览器兼容性不好（不建议用）</h3><br />
    <a href="jcropDemo.aspx">图片裁剪</a><br />
    <a href="ImageCutDemo.aspx">图片裁剪对话框</a>
    <br />
    <br />
    <a href="uploadifyDemo.aspx">文件图片上传</a>
    <br />
    <a href="UploadImageAndCut.aspx">图片上传并截图（页面中有2个弹出窗口版本，一个是本页上传并弹出截图窗口，一个是本页上传本页截图）</a><br />
    <h3>以下用的上传插件对浏览器兼容性不好（建议用）</h3>
    <p><a href="Webuploader/Demo.aspx">webUploader上传演示</a></p>
    <p>&nbsp;</p><br />

<h3>注册邮箱验证</h3>   
    <a href="User/UserRegister.aspx">注册并发邮件链接</a> 
    <br />
    <br />

<h3>网页打印</h3>   
    <a href="WebPrint.aspx">网页打印</a><br />
    <hr />
    <h3>图片缩放预览</h3>   
    <a href="ImageZoom.html">ImageZoom.html</a>
</asp:Content>
