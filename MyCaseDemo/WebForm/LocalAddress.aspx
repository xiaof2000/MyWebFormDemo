﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LocalAddress.aspx.cs" Inherits="WebForm.LocalAddress" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            位置测试
            <button id="btn1" type="button">定位</button>
        </div>
    </form>
    <script src="Scripts/jquery-1.8.2.min.js"></script>
    <script type="text/javascript">
        $(document).bind("pageinit", function (event, data) {   
            getLocation();
        });

        $("#btn1").click(function () {
            getLocation();
        })

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                                         showPosition,//成功回调函数
                                        getPositionError,//失败回调函数
                                            {//options参数配置
                                                enableHighAccuracy: true,//boolean 是否要求高精度的地理信息 
                                                timeout: 2000,
                                                maximumAge: 36000
                                            }
        );
            }
            else {
                alert("不支持");
                //不支持，就拉倒吧。 }
            }
        };
            //失败回调函数：
            function getPositionError(error) {

                switch (error.code) {

                    case error.TIMEOUT:

                        alert("连接超时，请重试");

                        break;

                    case error.PERMISSION_DENIED:

                        alert("您拒绝了使用位置共享服务，查询已取消");

                        break;

                    case error.POSITION_UNAVAILABLE:

                        alert("亲爱的火星网友，非常抱歉，我们暂时无法为您所在的星球提供位置服务");

                        break;

                }

            }

            //成功回调函数：
            function showPosition(position) {
                //内容在下面说。
                alert("成功调用" + position.latitude + "---" + position.longitude);
            }
        
    </script>
</body>
</html>
