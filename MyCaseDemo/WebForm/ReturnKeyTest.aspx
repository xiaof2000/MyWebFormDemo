﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReturnKeyTest.aspx.cs" Inherits="WebForm.ReturnKeyTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
     
    
</head>
<body>
    <form id="form1" runat="server">
        <h2>除过1个文本框回车时调用按钮1，外其它文本框回车时调用按钮2</h2>
        <div >这时只要将按钮1的回车相应（即提交响应属性：UseSubmitBehavior 设置为flash），并在要回车提交按钮1的文本框中写js就可</div>
    <div>
    
        <asp:TextBox ID="TextBox1" runat="server">相应按钮1</asp:TextBox>
        <asp:TextBox ID="TextBox3" runat="server">相应按钮 2</asp:TextBox>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="按钮1 UseSubmitBehavior设置为false" UseSubmitBehavior="False" />
        <asp:TextBox ID="TextBox4" runat="server">相应按钮 2</asp:TextBox>
        <br />
        <br />
        <asp:TextBox ID="TextBox2" runat="server">相应按钮 2</asp:TextBox>
        <asp:TextBox ID="TextBox5" runat="server">相应按钮 2</asp:TextBox>
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="按钮2 不用修改属性"  />
    <asp:TextBox ID="TextBox6" runat="server">相应按钮 1</asp:TextBox>
    </div>
    <script src="Scripts/jquery-1.8.2.js"></script>
    <script type="text/javascript">
       <%-- $("#<%= TextBox1.ClientID %>").keydown(function () {
            if (event.which || event.keyCode) {
                if ((event.which == 13) || (event.keyCode == 13)) {
                    document.getElementById('<%= Button1.ClientID %>').click();
                return false;
            }
        }
        else { return true };
        });--%>


        $("#TextBox1,#TextBox6").keydown(function () {
            if (event.which || event.keyCode) {
                if ((event.which == 13) || (event.keyCode == 13)) {
                    document.getElementById('Button1').click();
                    return false;
                }
            }
            else {
                return true
            };
        })
    </script>
    
        <p>
            结果：<asp:Label ID="Label1" runat="server" Font-Size="15pt" Text="Label"></asp:Label>
        </p>
    </form>
        
</body>
</html>
