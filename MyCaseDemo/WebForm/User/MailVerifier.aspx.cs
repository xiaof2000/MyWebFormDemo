﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForm.User
{
    public partial class MailVerifier : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string webId = Request.Params["d"];
            string webMail = Request.Params["f"];
            string webGuid = Request.Params["k"]; //这个暂时没用
            string content = string.Empty; //提示文字

            //按Id查邮箱，然后对邮箱进行md5加密后和参数中的mail比对
            
            //进行最基本的合法性检查
            if (webId==null || webMail==null || webGuid==null)
            {
                litText.Text  = "验证信息有误，请在您收到的邮件中进行验证！";
                return;
            }

            //在数据库中找到的邮箱地址
            
            string mail = "abc@126.com";


            
            string checkMail = Common.MD5Helper.GetMD5(mail, webId + "password");

            if (checkMail.ToLower()==webMail.ToLower())
            {
                //验证通过
                content = "<h3>邮箱验证成功！</h3><p>现在，您可以登录网站上传其它资料，并等待网站客服审核您的注册资料，审核通过后，您的网站将正式开通。</p><p>审核情况我们将通过邮件发送通知，请注意查收！</p>";
                //。在此期间，您可以收集整理您的公司介绍及产品图片、介绍等信息，
            }
            else
            {
                content = "<h3>邮箱验证失败！</h3><p>请确认您的验证信息是否正确！如多次验证失败，请与网站客服联系。</p>";
            }
            litText.Text = content;
        }
    }
}