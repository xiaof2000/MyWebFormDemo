﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MailVerifier.aspx.cs" Inherits="WebForm.User.MailVerifier" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h3>邮箱验证结果</h3>
        <div >
            验证结果如下<br />
            <asp:Literal ID="litText" runat="server"></asp:Literal>
            <hr />
            <p>如有其它疑问，请<a href='http://wpa.qq.com/msgrd?v=3&uin=1179050917&site=qq&menu=yes' target='_blank'>联系客服</a></p>
        </div>
    </div>
    </form>
</body>
</html>
