﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserRegister.aspx.cs" Inherits="WebForm.User.UserRegister" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h3>用户注册与激活</h3>
        用户注册后会发一封邮件<br />
        请输入您的邮箱:<asp:TextBox ID="txtMail" runat="server">abc@126.com</asp:TextBox><br />
        <asp:Button ID="btnRegister" runat="server" Text="注册" OnClick="btnRegister_Click" Width="61px" />
        <hr />
        加密后的信息<br />
        <asp:Label ID="lblText" runat="server" Text="Label"></asp:Label>
        <hr />
        <br />
        邮件链接：<asp:Literal ID="litText" runat="server"></asp:Literal>
    </div>
    </form>
</body>
</html>
