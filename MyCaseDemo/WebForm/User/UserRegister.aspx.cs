﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForm.User
{
    public partial class UserRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            //按用户id，用户邮箱和guid产生验证码

            string checkCode = GetCheckText("1", txtMail.Text , Guid.NewGuid().ToString("N"));
            lblText.Text = checkCode;

            string w = "http://localhost:50569/User/MailVerifier.aspx?";
            litText.Text = string.Format("<a href='{0}{1}' target='_blank'>邮箱验证</a>",w, checkCode);

           
        }


        /// <summary>
        /// 获取发送的验证字符串
        /// </summary>
        /// <param name="id">用户Id</param>
        /// <param name="mail">邮箱地址</param>
        /// <param name="checkNo">随机数</param>
        /// <returns></returns>
        private string GetCheckText(string id, string mail,string checkNo)
        {
            //checkNo 是扰乱码，没有用
            string checkMail =Common.MD5Helper.GetMD5(mail, id + "password");
            return string.Format("d={0}&f={1}&k={2}", id, checkMail,checkNo);
        }

    
    
    }
}