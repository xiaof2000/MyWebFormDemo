﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ValidateInput.aspx.cs" Inherits="WebForm.ValidateInput" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .auto-style2 {
            width: 406px;
        }
        .auto-style3 {
            width: 215px;
        }
        label.error {
            color: Red;
            font-size: 13px;
            margin-left: 5px;
            padding-left: 16px;
        }
    </style>
    <script src="Scripts/jquery-1.8.2.js"></script>
    <script src="Scripts/jquery.validate.js"></script>
    <script src="Scripts/jquery.validate_zh.js"></script>
</head>
<body>
    <h1 >输入验证</h1>
    <form id="form1" runat="server">
        <div>
            <table style="width: 98%">
                <tr>

                    <td style="text-align: right" class="auto-style3">*姓名：</td>
                    <td class="auto-style2">
                        <asp:TextBox ID="txtOrderingManName" MaxLength="30" runat="server" Width="180px" placeholder="请输入您的姓名"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: right" class="auto-style3">*手机：</td>
                    <td class="auto-style2">
                        <asp:TextBox ID="txtMPhone" MaxLength="30" runat="server" Width="160px" placeholder="请输入您的手机号"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: right" class="auto-style3">*性别：</td>
                    <td class="auto-style2">
                        <asp:DropDownList ID="cmbSex" runat="server" Width="166px">
                            <asp:ListItem Value="-1">&lt;请选择&gt;</asp:ListItem>
                            <asp:ListItem Value="1">男</asp:ListItem>
                            <asp:ListItem Value="0">女</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: right" class="auto-style3">Mail：</td>
                    <td class="auto-style2">
                        <asp:TextBox ID="txtMail" MaxLength="30" runat="server" Width="180px"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: right" class="auto-style3">*留言：</td>
                    <td class="auto-style2">
                        <asp:TextBox ID="txtTheMessage" MaxLength="200" runat="server" Width="500px" Height="68px" TextMode="MultiLine" placeholder="请输入您所需要的产品数据、质量要求等"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style3">

                        <asp:Label ID="lblMsg" runat="server" BackColor="Transparent" ForeColor="Red"></asp:Label>
                        <asp:Button ID="btnSumbit" runat="server" Text="提 交" OnClick="btnSumbit_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="color: Red;">请您文明上网、理性发言！</span>
                    </td>
                </tr>
            </table>
        </div>
    </form>
    <script type="text/javascript">
        $(function () {

            $("#form1").validate({
                rules: {
                    txtOrderingManName: {
                        required: true,
                        minlength: 2
                    },
                    txtMPhone: {
                        required: true,
                        isMobile: true
                    },
                    txtMail: {
                        email: true
                    },
                    txtTheMessage: {
                        required: true,
                        minlength: 5,
                        maxlength:200
                    },
                    cmbSex: //
                        { min:0}
                        //{
                        //required:function(element){
                        //    if (1 == 2)
                        //        return true;
                        //    else
                        //        return false;
                        //    }
                        //}
                },
                messages:{
                    cmbSex:{min:'请选择一项'}
            }

            });
        });

    </script>
</body>
</html>
