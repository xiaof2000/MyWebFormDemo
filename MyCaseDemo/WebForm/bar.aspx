﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bar.aspx.cs" Inherits="WebForm.bar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="Scripts/jquery-1.8.2.js"></script>
    <script src="Scripts/progressBar/jquery.progressbar.min.js"></script>
    <style type="text/css">
        /* General Links */
        a:link {
            text-decoration: none;
            color: #3366cc;
            border: 0px;
        }

        a:active {
            text-decoration: underline;
            color: #3366cc;
            border: 0px;
        }

        a:visited {
            text-decoration: none;
            color: #3366cc;
            border: 0px;
        }

        a:hover {
            text-decoration: underline;
            color: #ff5a00;
            border: 0px;
        }

        img {
            padding: 0px;
            margin: 0px;
            border: none;
        }

        body {
            margin: 0 auto;
            width: 100%;
            font-family: 'Verdana';
            color: #40454b;
            font-size: 12px;
            text-align: center;
        }



        .content {
            margin: 20px;
            line-height: 20px;
        }

        body h1 {
            font-size: 14px;
            font-weight: bold;
            color: #CC0000;
            padding: 5px;
            border-bottom: solid;
            border-bottom-width: 1px;
            border-bottom-color: #333333;
        }

        body h2 {
            font-size: 14px;
            font-weight: bold;
            color: #CC0000;
            padding: 5px;
            border-bottom: solid;
            border-bottom-width: 1px;
            border-bottom-color: #333333;
        }

        #demo {
            margin: 0 auto;
            width: 100%;
        }

            #demo .extra {
                padding-left: 30px;
            }

            #demo .options {
                padding-left: 10px;
            }

            #demo .getOption {
                padding-left: 10px;
                padding-right: 20px;
            }
    </style>
</head>
<body>

    <form id="form1" runat="server">
     
        <div id="progress1"></div><span> 20人评价 占20%</span>
        <div id="progress2"></div>
        <div id="progress3"></div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#progress1").progressBar(60/2, { barImage: '/Scripts/progressBar/percentImage_back4.png' ,showText:false});
            $("#progress2").progressBar(31, { barImage: '/Scripts/progressBar/percentImage_back4.png',boxImage:'' });
            $("#progress3").progressBar(29, { barImage: '/Scripts/progressBar/percentImage_back4.png' });

        });
    </script>

</body>
</html>
