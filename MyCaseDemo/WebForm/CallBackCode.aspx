﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CallBackCode.aspx.cs" Inherits="WebForm.CallBackCode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="Scripts/jquery-1.8.2.min.js"></script>
     <script type="text/javascript">

         $("#testid").click(function () {
             test();
         });

         function test() {
             alert("这是一个弹出框！");
         };


         $("#txtBox").keydown(function () {
             if (event.which || event.keyCode) {
                 if ((event.which == 13) || (event.keyCode == 13)) {
                     //ok document.getElementById("Button1").click();
                     document.getElementById('<%= Button1.ClientID %>').click();
                    return false;
                }
            }
            else { return true };
        });
    </script>
</head>
<body>

    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="txtBox" runat="server"></asp:TextBox>
            <asp:Button ID="btnOk" runat="server" Text="Button" OnClick="Button1_Click" />
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click1" Text="Button" />
            <br />
            <br />
            <br />
            另一个测试<br />
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            &nbsp;<asp:Button ID="btnOther" runat="server" Text="Button" OnClick="btnOther_Click" />
            <br />
            <br />
            <hr />
            <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="后台调用前台" />
            &nbsp;
            <a id="testid" href="#" >前台调用js</a>
        </div>
    </form>
   
</body>
</html>
