﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;

namespace WebForm
{
    public partial class Log4NetTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //df test 写日志的测试代码
            //ILog logs = LogHelper.GetInstance("LogFileAppenderByDate_Info");
            //logs.Fatal("这是一个测试");

            log4net.ILog log = log4net.LogManager.GetLogger("EMailLog");//System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            log.Error("新测试error---EMail");
            log.Debug("新测试debug---EMail");
            log.Info("新测试info---EMail");


            //写系统运行日志
             log4net.ILog log1 = log4net.LogManager.GetLogger("SystemLog");//System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
             log1.Error("新测试error");
             log1.Debug("新测试debug");
             log1.Info("新测试info");
            
        }
    }
}