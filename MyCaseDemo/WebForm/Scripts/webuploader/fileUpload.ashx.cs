﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace WebForm.Scripts.webuploader
{
    /// <summary>
    /// fileUpload 的摘要说明
    /// </summary>
    public class fileUpload : IHttpHandler, IRequiresSessionState
    {

        private string _uploadPath = "Update";
        public void ProcessRequest(HttpContext context)
        {
            
            context.Response.ContentType = "text/plain";
            context.Request.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");
            context.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");
            context.Response.Charset = "utf-8";

            string filePathName = string.Empty; //文件位置
            string privateFolder = string.Empty;

            //todo 加入权限验证代码
            //if ((context.Session["BusinessAdmin"] == null) && (context.Session["Admin"] == null))
            //{
            //    return;
            //}

            //接收上传后的文件
            HttpPostedFile file = context.Request.Files["file"];

            //获取文件的保存路径
            if (string.IsNullOrEmpty(context.Request.Params["folder"]))
            {
                _uploadPath = "Update";
            }
            else
            {
                _uploadPath = context.Request.Params["folder"].Replace("/", string.Empty);
            }

            //获取文件的保存子目录
            if (string.IsNullOrEmpty(context.Request.Params["privateFolder"]))
            {
                privateFolder = string.Empty;
                _uploadPath = _uploadPath + "\\";
            }
            else
            {
                privateFolder = context.Request.Params["privateFolder"].Replace("/", string.Empty);
                _uploadPath = _uploadPath + "\\" + privateFolder + "\\";
            }



            string localPath = Path.Combine(HttpRuntime.AppDomainAppPath, _uploadPath);

            if (context.Request.Files.Count == 0)
            {
                //return Json(new { jsonrpc = 2.0, error = new { code = 102, message = "保存失败" }, id = "id" });
                context.Response.Write("0");
            }

            string ex = Path.GetExtension(file.FileName);
            //filePathName = Guid.NewGuid().ToString("N") + ex;
            filePathName = DateTime.Now.ToString("yyMMddHHmmssffff") + ex;

            if (!System.IO.Directory.Exists(localPath))
            {
                System.IO.Directory.CreateDirectory(localPath);
            }
            string r = Path.Combine(localPath, filePathName);
            file.SaveAs(r);

            string json = string.Format("{{\"id\":{0},\"newName\":\"{1}\" }}", 1, filePathName);  //返回json数据 测试通过

            context.Response.Write(json);

            //            context.Response.Write(filePathName);

            //return Json(new
            //{
            //    jsonrpc = "2.0",
            //    id = id,
            //    filePath = "/Upload/" + filePathName
            //});

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}