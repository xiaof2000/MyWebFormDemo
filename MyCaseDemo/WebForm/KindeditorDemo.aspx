﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KindeditorDemo.aspx.cs" Inherits="WebForm.KindeditorDemo" ValidateRequest="False" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" href="/Scripts/Kindeditor/themes/default/default.css" />
	<link rel="stylesheet" href="/Scripts/Kindeditor/plugins/code/prettify.css" />
	<script charset="utf-8"src="Scripts/Kindeditor/kindeditor-min.js"></script>
    <%--<script charset="utf-8" src="/Scripts/Kindeditor/kindeditor.js"></script>
	<script charset="utf-8" src="/Scripts/Kindeditor/lang/zh_CN.js"></script>
	<script charset="utf-8" src="/Scripts/Kindeditor/plugins/code/prettify.js"></script>
	--%>
    <script>
	    KindEditor.ready(function (K) {
	        var editor1 = K.create('#txtContent', {
	            cssPath: '/Scripts/Kindeditor/plugins/code/prettify.css',
	            uploadJson: '/Scripts/Kindeditor/asp.net/upload_json.ashx',
	            fileManagerJson: '/Scripts/Kindeditor/asp.net/file_manager_json.ashx',
	            allowFileManager: true,
	            filterMode: false, //不过滤代码
	            items:
                [   'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
                    'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
                    'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
                    'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
                    'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'multiimage',
                    'flash', 'media', 'insertfile', 'table', 'hr', 'emoticons', 'baidumap', 'pagebreak',
                    'anchor', 'link', 'unlink']  ,
	            afterCreate: function () {
	                var self = this;
	                K.ctrl(document, 13, function () {
	                    self.sync();
	                    K('form[name=example]')[0].submit();
	                });
	                K.ctrl(self.edit.doc, 13, function () {
	                    self.sync();
	                    K('form[name=example]')[0].submit();
	                });
	            }
	        });
	        prettyPrint();
	    });
	</script>
</head>
<body>
    <form id="form1" runat="server">
        要将本页面的ValidateRequest设置为false
    <div>
      文本内容：<asp:TextBox ID="txtContent" runat="server" Width="580px"></asp:TextBox>
    </div>
        <asp:Button ID="Button1" runat="server" Text="Button" />
    </form>
</body>
</html>
