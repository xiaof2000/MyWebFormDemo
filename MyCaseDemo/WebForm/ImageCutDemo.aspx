﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImageCutDemo.aspx.cs" Inherits="WebForm.ImageCutDemo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>图片裁剪对话框演示</title>
    <script src="Scripts/jquery-1.8.2.min.js"></script>
    <script src="Scripts/plugin_layer/layer.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <a id="openImage" href="#">打开对话框（有预览图）</a><br />
            <a id="openImage1" href="#">打开对话框截宽图（有预览图）</a>
            <hr />
            <a id="openImage3" href="#">打开对话框截</a>
            <a id="openImage4" href="#">打开对话框截宽图</a>
            <br />
            
        </div>
    </form>
    <script type="text/javascript">
        $("#openImage").click(function () {
            $.layer({
                type: 2,
                title: '图片裁剪',
                shadeClose: true, //开启点击遮罩关闭层
                area: ['750px', '500px'],
                offset: ['100px', ''],
                iframe: { src: 'ImageCut.aspx?newWidth=300&newHeight=300' }
            });
        });

        $("#openImage1").click(function () {
            $.layer({
                type: 2,
                title: '图片裁剪111',
                shadeClose: true, //开启点击遮罩关闭层
                area: ['750px', '500px'],
                offset: ['100px', ''],
                iframe: { src: 'ImageCut.aspx?newWidth=200&newHeight=100' }
            });
        });

        $("#openImage3").click(function () {


            $.layer({
                type: 2,
                title: '图片裁剪',
                maxmin: true,
                shadeClose: true, //开启点击遮罩关闭层
                area: ['700px', '550px'],
                offset: ['100px', ''],
                iframe: { src: 'ImageCutNoPreview.aspx?newWidth=300&newHeight=300&random=' + sj }
            });
        });




        $("#openImage4").click(function () {


            $.layer({
                type: 2,
                maxmin: true,
                title: '图片裁剪',
                shadeClose: true, //开启点击遮罩关闭层
                area: ['700px', '550px'],
                offset: ['100px', ''],
                iframe: { src: 'ImageCutNoPreview.aspx?newWidth=200&newHeight=100' }
            });
        });



    </script>
</body>
</html>
