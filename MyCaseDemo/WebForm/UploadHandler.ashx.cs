﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace WebForm
{
    /// <summary>
    /// 文件上传
    /// </summary>
    public class UploadHandler : IHttpHandler, IRequiresSessionState
    {
        string _defaultFilePath = "UploadFiles";
        public void ProcessRequest(HttpContext context)
        {

            //if ((context.Session["BusinessAdmin"] == null) && (context.Session["Admin"] == null))
            //{
            //    return;
            //}


            context.Response.ContentType = "text/plain";
            context.Request.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");
            context.Response.ContentEncoding = System.Text.Encoding.GetEncoding("UTF-8");
            context.Response.Charset = "utf-8";
            //http://www.cnblogs.com/babycool/
            //接收上传后的文件
            HttpPostedFile file = context.Request.Files["Filedata"];
            //其他参数
            //string somekey = context.Request["someKey"];
            //string other = context.Request["someOtherKey"];

            //获取文件的保存路径
            if (string.IsNullOrEmpty(context.Request.Params["folder"]))
            {
                _defaultFilePath = "UploadFiles";
            }
            else
            {
                _defaultFilePath = context.Request.Params["folder"];
            }


            string privateFolder = string.Empty;
            //获取文件的保存子目录
            if (string.IsNullOrEmpty(context.Request.Params["privateFolder"]))
            {
                privateFolder = string.Empty;
                _defaultFilePath = HttpContext.Current.Server.MapPath(_defaultFilePath + "\\");
            }
            else
            {
                privateFolder = context.Request.Params["privateFolder"];
                _defaultFilePath = HttpContext.Current.Server.MapPath(_defaultFilePath + "\\" + privateFolder + "\\");
            }


            //判断上传的文件是否为空
            if (file != null)
            {
                if (!Directory.Exists(_defaultFilePath))
                {
                    Directory.CreateDirectory(_defaultFilePath);
                }

                //保存文件
                //file.SaveAs(uploadPath + file.FileName);

                //这里要用自己的修改的名字 并加入用户是否已登录系统的权限判断
                string newName = DateTime.Now.ToString("yyMMddHHmmssffff") + GetExtensionName(file.FileName, true);

                //修改成新文件名
                file.SaveAs(_defaultFilePath + newName);

                //context.Response.Write("1");
                context.Response.Write(newName);//输出新文件名
            }
            else
            {
                context.Response.Write("0");
            }
        }

        /// <summary>
        /// 获取文件扩展名
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="isReturnPeriod">返回.号不</param>
        /// <returns></returns>
        private string GetExtensionName(string filename, bool isReturnPeriod)
        {
            int i = filename.IndexOf(".");
            string ext = string.Empty;
            //如果找到了并且不是最后一个字符
            if (i > 0 && i < filename.Length)
            {
                ext = filename.Substring(i + 1);
            }
            if (string.IsNullOrEmpty(ext))
            {
                return string.Empty;
            }
            if (isReturnPeriod)
            {
                return "." + ext;
            }
            else
            {
                return ext;
            }


        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}