﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="gundong.aspx.cs" Inherits="WebForm.gundong" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>滚动图片</title>

    <script src="Scripts/jquery-1.8.2.min.js"></script>
    <script src="Scripts/jCarouselOld/jquery.jcarousel.pack.js"></script>
    <link href="Scripts/jCarouselOld/jquery.jcarousel.css" rel="stylesheet" />
    <link href="Scripts/jCarouselOld/skins/jieshao/skin.css" rel="stylesheet" /> <%--我加入的样式--%>
      <link href="Scripts/jCarouselOld/skins/tango/skin.css" rel="stylesheet" /> <%--插件原来提供的样式--%>
    <script type="text/javascript">

        jQuery(document).ready(function () {
            jQuery('#mycarouselNew').jcarousel({
                scroll: 5 //每次滚动的数量
            });

            jQuery('#mycarousel').jcarousel({
                scroll: 1 //每次滚动的数量
            });
        });

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div id="wrap">
            <h1>jCarousel</h1>
            <h2>jQuery下的滚动切换传送插件 当前设置的大小为860 * 158</h2>
            
  <h3>我新定义的样式，加入的标题和介绍文字</h3>
          

            <ul id="mycarouselNew" class="jcarousel-skin-jieshao">
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="140" height="90" alt="" /></a>
                    <div class="title"><a href="#">产品名称1</a> </div>
                    <div class="description">产品介绍的产品介绍的产品介绍的产品介绍的产品介绍的</div>

                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="140" height="90" alt="" /></a>
                    <div class="title"><a href="#">产品名称2</a> </div>
                    <div class="description">产品介绍产品介绍产品介绍产品介绍产品介绍</div>
                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="140" height="90" alt="" /></a>
                    <div class="title"><a href="#">产品名称3</a> </div>
                    <div class="description">产品介绍产品介绍产品介绍产品介绍产品介绍</div>
                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="140" height="90" alt="" /></a>
                    <div class="title"><a href="#">产品名称4</a> </div>
                    <div class="description">产品介绍产品介绍产品介绍产品介绍产品介绍</div>
                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="140" height="90" alt="" /></a>
                    <div class="title"><a href="#">产品名称5</a> </div>
                    <div class="description">产品介绍产品介绍产品介绍产品介绍产品介绍</div>
                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="140" height="90" alt="" /></a>
                    <div class="title"><a href="#">产品名称6</a> </div>
                    <div class="description">产品介绍产品介绍产品介绍产品介绍产品介绍</div>

                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="140" height="90" alt="" /></a>
                    <div class="title"><a href="#">产品名称7</a> </div>
                    <div class="description">产品介绍产品介绍产品介绍产品介绍产品介绍</div>
                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="140" height="90" alt="" /></a>
                    <div class="title"><a href="#">产品名称8</a> </div>
                    <div class="description">产品介绍产品介绍产品介绍产品介绍产品介绍</div>
                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="140" height="90" alt="" /></a>
                    <div class="title"><a href="#">产品名称9</a> </div>
                    <div class="description">产品介绍产品介绍产品介绍产品介绍产品介绍</div>
                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="140" height="90" alt="" /></a>
                    <div class="title"><a href="#">产品名称10</a> </div>
                    <div class="description">产品介绍产品介绍产品介绍产品介绍产品介绍</div>
                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="140" height="90" alt="" /></a>
                    <div class="title"><a href="#">产品名称</a> </div>
                    <div class="description">产品介绍产品介绍产品介绍产品介绍产品介绍</div>
                </li>


            </ul>


            <h2 >原插件的样式</h2>
            <ul id="mycarousel" class="jcarousel-skin-tango">
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="75" height="75" alt="" /></a>
                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="75" height="75" alt="" /></a>
                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="75" height="75" alt="" /></a>
                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="75" height="75" alt="" /></a>
                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="75" height="75" alt="" /></a>
                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="75" height="75" alt="" /></a>
                </li>
                <li>
                    <a href="#"><img src="Scripts/jCarouselOld/01.jpg" width="75" height="75" alt="" /></a>
                </li>
            </ul>
        </div>



    </form>


</body>
</html>
