﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebForm.Common
{
    /// <summary>
    /// 文件操作助手类
    /// </summary>
    public static class FileHelper
    {
        /// <summary>
        /// 获取没有目录和网址参数的文件名，如：http://www.sohu.com/123.jpg?time=123456获取后为123.jpg
        /// </summary>
        /// <param name="fileName"></param>
        public static string GetFileTrueName(string fileName)
        {
            if (string.IsNullOrEmpty(fileName.Trim()))
            {
                return string.Empty;
            }

            //剔除文件目录名等
            int j = fileName.LastIndexOf("/");
            if (j != -1)
            {
                fileName = fileName.Substring(j + 1, fileName.Length - j - 1);
            }

            //剔除随机数
            j = fileName.LastIndexOf("?");
            if (j != -1)
            {
                fileName = fileName.Substring(0, j);
            }
            return fileName;
        }



        /// <summary>
        /// 获取文件扩展名
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="isReturnPeriod">返回.号不</param>
        /// <returns></returns>
        public static string GetExtensionName(string fileName, bool isReturnPeriod)
        {
            if (string.IsNullOrEmpty(fileName.Trim()))
            {
                return string.Empty;
            }

            int i = fileName.IndexOf(".");
            string ext = string.Empty;
            //如果找到了并且不是最后一个字符
            if (i > 0 && i < fileName.Length)
            {
                ext = fileName.Substring(i + 1);
            }
            if (string.IsNullOrEmpty(ext))
            {
                return string.Empty;
            }
            if (isReturnPeriod)
            {
                return "." + ext;
            }
            else
            {
                return ext;
            }


        }
    }
}