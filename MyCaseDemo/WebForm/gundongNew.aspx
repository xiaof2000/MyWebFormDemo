﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="gundongNew.aspx.cs" Inherits="WebForm.gundongNew" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="Scripts/jCarousel/style.css" rel="stylesheet" />
    <link href="Scripts/jCarousel/jcarousel.responsive.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.8.2.min.js"></script>
    <script src="Scripts/jCarousel/jquery.jcarousel.min.js"></script>

    <%--插件说明文档地址 http://sorgalla.com/jcarousel/docs/--%>

    <script type="text/javascript">

        jQuery(document).ready(function () {

            jQuery('.jcarousel').jcarousel({
                wrap: 'circular', //最后一个的切换方式
                animation: {
                    duration: 800,
                    easing: 'linear',
                }
            });

            //自动滚动
            $(".jcarousel").jcarouselAutoscroll({

                interval: 2000
                
            });

        $('.jcarousel-control-prev')
            .on('jcarouselcontrol:active', function () {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function () {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
              target: '-=1'
        });

        $('.jcarousel-control-next')
            .on('jcarouselcontrol:active', function () {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function () {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
               target: '+=3' //每次滚动3张图片
         });

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <h1>Responsive Carousel</h1>


            <p>This example shows how to implement a responsive carousel. Resize the browser window to see the effect.</p>

            <div class="jcarousel-wrapper">
                <div class="jcarousel">
                    <ul>
                        <li>
                            <img src="Images/jcarousel_demo.png" width="75" height="75" alt="" /></li>
                        
                        <li>
                            <img src="Images/jcarousel_demo.png" width="75" height="75" alt="" /></li>
                        <li>
                            <img src="Images/jcarousel_demo.png" width="75" height="75" alt="" /></li>
                        <li>
                            <img src="Images/jcarousel_demo.png" width="75" height="75" alt="" /></li>
                        <li>
                            <img src="Images/jcarousel_demo.png" width="75" height="75" alt="" /></li>
                        <li>
                            <img src="Scripts/jCarouselOld/png-0006.png" width="75" height="75" alt="" /></li>
                    </ul>
                </div>

                <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                <a href="#" class="jcarousel-control-next">&rsaquo;</a>

                <p class="jcarousel-pagination"></p>
            </div>
        </div>
    </form>
</body>
</html>
