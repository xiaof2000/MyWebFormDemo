﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ZTreeTest.aspx.cs" Inherits="WebForm.ZTreeTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="Scripts/zTree/css/demo.css" rel="stylesheet" />
    <link href="Scripts/zTree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" />
    <script src="Scripts/zTree/js/jquery-1.4.4.min.js"></script>
    <script src="Scripts/zTree/js/jquery.ztree.all-3.5.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="selectShow">点我选择</div>
        <div id="selectId">选择的Id</div>
        <div id="selectText">选择的Text</div>
       <%-- <input type="text" id="key" class="Side_Toput2" name="name" value="" /><input type="button" value="查找" onclick="searchNode();" />--%>
        <ul id="treeDemo" class="ztree"></ul>
    </form>
    <script type="text/javascript">

        //$("#selectShow").click(function(){
        //    InitTree();
        //});



        ///初始化树的

        var setting = {
            check: {
                enable: true,
                chkboxType: { "Y": "s", "N": "s" } //请注意大小写，不要改变。Y 属性定义 checkbox 被勾选后的情况； N 属性定义 checkbox 取消勾选后的情况； "p" 表示操作会影响父级节点； "s" 表示操作会影响子级节点。

            },
            //view: {
            //    fontCss: getFontCss,
            //    dblClickExpand: false,
            //    showLine: false
            //},
            data: {
                //key: {
                //    title: "t"
                //},
                simpleData: {
                    enable: true
                    //checked: true
                }
            },
            callback: {
                onClick: onClick,
                onCheck: nodeCheck
            }
        };

        //请求树状数据，初始化树
        var zNodes = [
        { id: 1, pId: 0, name: "父节点1 - 展开", open: true },
        { id: 2, pId: 0, name: "父节点1开", open: true },
        { id: 11, pId: 1, name: "父节点11 - 折叠" }];
        //返回的数据格式,如上，注意一点自己拼接字符串返回的会有问题

        //树的单击事件
        function onClick(e, treeId, treeNode) {

            //看当前是不是叶子节点 if (treeNode.isParent ){

            //这是单机节点展开
            //var zTree = $.fn.zTree.getZTreeObj("leftTree");
            //zTree.expandNode(treeNode);
            //判断是否是ie浏览器，对iframe赋值
            var kws = treeNode.name;
            var id = treeNode.id;
            $("#selectId").text(id);
            $("#selectText").text(kws);

            //var url = "BigDataIndex.aspx?prokw=" + escape(kws) + "&entname=" + escape(kws) + "&time=" + new Date().getTime();

            ////由于ie和火狐，其他浏览器不同，ie支持location,貌似不支持src,不知道是什么原因，所以做了判断，可能是iframe的原因吧，没做深究，
            ////有大神可以告诉我一下
            //if ($.browser.msie) {
            //    ///获取iframe的id
            //    window.parent.frames['FlashData'].location = url;
            //}
            //else {
            //    window.parent.frames['FlashData'].src = url;
            //}
        }

        ////获取选择的id
        //$("#btnAddChildren,#btnEdit").click(function () {

        //    var zTree = $.fn.zTree.getZTreeObj("treeDemo");
        //    var nodes = zTree.getSelectedNodes();
        //    if (nodes.length > 0) {
        //        $("#hfdId").val(nodes[0].id);

        //    }
        //    else {
        //        $("#hfdId").val("");
        //        alert("请先选择数据后操作！")
        //        return false;
        //    }
        //});



        function nodeCheck(event, treeId, treeNode) {
            //alert("NodeId:" + treeNode.id + " " + treeNode.tId + ", " + treeNode.name + "," + treeNode.checked);

            var zTree = $.fn.zTree.getZTreeObj("treeDemo");
            var nodes = zTree.getCheckedNodes();
            var ids="";
            for (var i = 0, l = nodes.length; i < l; i++) {
                //treeObj.checkNode(nodes[i], true, true);
                ids = ids + nodes[i].id + ",";
            }

            if (ids.length > 1) {
                ids = ids.substr(0,ids.length - 1)
            }
            
            $("#selectId").text(ids);
        };
    
        var removeId = 0;

        ////获取url中的参数
        //function getUrlParam(name) {
        //    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        //    var r = window.location.search.substr(1).match(reg);
        //    if (r != null) return unescape(r[2]); return null;
        //}

        ////删除此id下的所有结点
        //function removeThisId(id) {
        //    var treeobj = $.fn.zTree.getZTreeObj("treeDemo");

        //    var node = treeobj.getNodeByParam("id", id, null);
        //    if (node == null) {
        //    }
        //    else {
        //        treeobj.removeNode(node);
        //    }
        //}

        $(document).ready(function () {
            $.fn.zTree.init($("#treeDemo"), setting, zNodes); //初始化树

            ////如果是修改时打开本窗口就不显示url中的id和它的下级
            //removeId = getUrlParam('id');
            //if (removeId == null) {
            //    removeId = 0;
            //}
            //removeThisId(removeId);
        });

        //$.ajax({
        //    type: 'Get',
        //    url: '?rootid=0',
        //    dataType: "json", //可以是text，如果用text，返回的结果为字符串；如果需要json格式的，可是设置为json
        //    success: function (data) {
        //        $.fn.zTree.init($("#leftTree"), setting, data);

        //    },
        //    error: function (msg) {

        //        alert(" 数据加载失败！" + msg);
        //    }
        //});

    </script>
</body>
</html>
