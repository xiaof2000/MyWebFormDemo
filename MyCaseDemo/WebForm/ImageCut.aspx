﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImageCut.aspx.cs" Inherits="WebForm.ImageCut" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>图片裁剪</title>
    <link href="Scripts/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet" />
     <style type="text/css">
        

        /* Apply these styles only when #preview-pane has
   been placed within the Jcrop widget */
        .jcrop-holder #preview-pane {
            display: block;
            position: absolute;
            z-index: 2000;
            top: 10px;
            right: -280px;
            padding: 6px;
            border: 1px rgba(0,0,0,.4) solid;
            background-color: white;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border-radius: 6px;
            -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
            -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
            box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
        }

        /* The Javascript code will set the aspect ratio of the crop
   area based on the size of the thumbnail preview,
   specified here */
        #preview-pane .preview-container {
            width: 200px;
            height: 100px;
            overflow: hidden;
        }

        /*设置工具栏样式*/
        * html,* html body /* 修正IE6振动bug */{background-image:url(about:blank);background-attachment:fixed;}
        * html .fixed-bottom /* IE6 底部固定 */{position:absolute;bottom:auto;top:expression(eval(document.documentElement.scrollTop+document.documentElement.clientHeight-this.offsetHeight-(parseInt(this.currentStyle.marginTop,10)||0)-(parseInt(this.currentStyle.marginBottom,10)||0)));}
        
        #toolBar {
            height:50px;
            width:100%;
            background-color:#ffd800;
            position:fixed;
            bottom:0;
        }

        #toolBarPanel {
            float:right;
            width:200px;
            line-height:50px;
            background-color:#808080;
        }
      
    </style>
    <script src="Scripts/jquery-1.8.2.min.js"></script>
    <script src="Scripts/jcrop/js/jquery.Jcrop.min.js"></script>

    <script type="text/javascript">

    
        function closeBox() {
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
        };

        jQuery(function ($) {
            var jcrop_api,
                boundx,
                boundy,

                // Grab some information about the preview pane
                $preview = $('#preview-pane'),
                $pcnt = $('#preview-pane .preview-container'),
                $pimg = $('#preview-pane .preview-container img'),
               // xsize = $pcnt.width(),
                //ysize = $pcnt.height();
                xsize = <%= _sizeWidth %>,
                ysize = <%= _sizeHeight %>;

            //根据传入的宽高比设置预览图宽高
            $pcnt.css({
                width: xsize + 'px',
                height: ysize + 'px'
            });

            console.log('init', [xsize, ysize]);//输出宽高数值

                        
            $('#target').Jcrop({

                //setSelect: [ 0, 0, 540, 330 ],//默认加上选择框
                boxWidth:450, //设置宽度
                onChange: updatePreview,
                onSelect: updatePreview,
                
                aspectRatio: xsize / ysize
            }, function () {
                // Use the API to get the real image size
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];
                // Store the API in the jcrop_api variable
                jcrop_api = this;

                // Move the preview into the jcrop container for css positioning
                $preview.appendTo(jcrop_api.ui.holder);
            });

            function updatePreview(c) {
                //if (parseInt(c.w) > 0) {
                //    var rx = xsize / c.w;
                //    var ry = ysize / c.h;
                //    var _width;
                //    var _height;
                   
                //    if (Math.round(rx * boundx) > $preview.width()) {
                //        _width = $preview.width();
                //    }
                //    else {
                //        _width = Math.round(rx * boundx);
                //    }

                //    if (Math.round(ry * boundy) > $preview.height()) {
                //        _height = $preview.height();
                //    }
                //    else {
                //        _height = Math.round(ry * boundy);
                //    }

                //    $pimg.css({
                //        width: _width + 'px',
                //        height: _height + 'px',
                //        marginLeft: '-' + Math.round(rx * c.x) + 'px',
                //        marginTop: '-' + Math.round(ry * c.y) + 'px'
                //    });


                if (parseInt(c.w) > 0) {
                    var rx = xsize / c.w;
                    var ry = ysize / c.h;

                    //console.log('c.w c.h', [c.w, c.h]);//输出宽高数值
                    //console.log('parseSize', [rx * boundx, ry * boundx]);//输出宽高数值

                    $pimg.css({
                        width: Math.round(rx * boundx) + 'px',
                        height: Math.round(ry * boundy) + 'px',
                        marginLeft: '-' + Math.round(rx * c.x) + 'px',
                        marginTop: '-' + Math.round(ry * c.y) + 'px'
                    });

                    $('#x1').val(c.x);
                    $('#y1').val(c.y);
                    $('#x2').val(c.x2);
                    $('#y2').val(c.y2);
                    $('#Iwidth').val(c.w);
                    $('#Iheight').val(c.h);
                };
            };

            $("#btnClose").click(function () {
                closeBox();
            });          
        });
 
    </script>

   

</head>
<body>
    <form id="form1" runat="server">
        此页面中预览图大小按传入要截取的大小自动变化。这样就有个问题，如果要截成的图太大就会导致截图挡住了原图。。。
             <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="jc-demo-box">
                        <asp:Image ID="target" runat="server" class="jcrop-preview" alt="Preview" />
                        <div id="preview-pane">
                            <div class="preview-container">
                                <asp:Image ID="preview" runat="server" class="jcrop-preview" alt="Preview" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="x1" runat="server" />
        <asp:HiddenField ID="y1" runat="server" />
        <asp:HiddenField ID="x2" runat="server" />
        <asp:HiddenField ID="y2" runat="server" />
        <asp:HiddenField ID="Iwidth" runat="server" />
        <asp:HiddenField ID="Iheight" runat="server" />
        <div id="toolBar">
            <div id="toolBarPanel">
            <asp:Button ID="btnSaveImage" runat="server" Text="确定" OnClick="saveImg" Width="65px"  />
                &nbsp;&nbsp;
                <%--<asp:Button ID="btnClose" runat="server" Text="取消" OnClick="saveImg" Width="65px" CausesValidation="False"  />--%>
                <input type="button" id="btnClose"  title="取消" value="取消" style="width:65px"/>
                </div>
        </div>
    </form>
</body>
</html>
