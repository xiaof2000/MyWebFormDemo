﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImageCutNoPreview.aspx.cs" Inherits="WebForm.ImageCutNoPreview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>图片裁剪</title>
    <link href="Scripts/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet" />
    <style type="text/css">

        /*设置工具栏样式*/
        * html, * html body /* 修正IE6振动bug */ {
            background-image: url(about:blank);
            background-attachment: fixed;
        }

        * html .fixed-bottom /* IE6 底部固定 */ {
            position: absolute;
            bottom: auto;
            top: expression(eval(document.documentElement.scrollTop+document.documentElement.clientHeight-this.offsetHeight-(parseInt(this.currentStyle.marginTop,10)||0)-(parseInt(this.currentStyle.marginBottom,10)||0)));
        }

        #toolBar {
            height: 50px;
            width: 100%;
            position: fixed;
            bottom: 0;
        }

        #toolBarPanel {
            float: right;
            width: 200px;
            line-height: 50px;
            
        }

    </style>
    <script src="Scripts/jquery-1.8.2.min.js"></script>
    <script src="Scripts/jcrop/js/jquery.Jcrop.min.js"></script>

    <script type="text/javascript">

        function closeBox() {
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
        };

        jQuery(function ($) {
            var jcrop_api,
                boundx,
                boundy,

                // Grab some information about the preview pane
                $preview = $('#preview-pane'),
                $pcnt = $('#preview-pane .preview-container'),
                $pimg = $('#preview-pane .preview-container img'),
               // xsize = $pcnt.width(),
                //ysize = $pcnt.height();
                xsize = <%= _sizeWidth %>,
                ysize = <%= _sizeHeight %>;


            console.log('init', [xsize, ysize]);//输出宽高数值
                        
            $('#target').Jcrop({

                //setSelect: [ 0, 0, 540, 330 ],//默认加上选择框
                boxWidth:680, //设置宽度
                onChange: updatePreview,
                onSelect: updatePreview,
                aspectRatio: xsize / ysize
            }, function () {
                // Use the API to get the real image size
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];
                // Store the API in the jcrop_api variable
                jcrop_api = this;

                // Move the preview into the jcrop container for css positioning
                $preview.appendTo(jcrop_api.ui.holder);
            });

            function updatePreview(c) {
                if (parseInt(c.w) > 0) {
                    $('#x1').val(c.x);
                    $('#y1').val(c.y);
                    $('#x2').val(c.x2);
                    $('#y2').val(c.y2);
                    $('#Iwidth').val(c.w);
                    $('#Iheight').val(c.h);
                };
            };

            $("#btnClose").click(function () {
                closeBox();
            });          
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="jc-demo-box">
                        <asp:Image ID="target" runat="server" class="jcrop-preview" alt="Preview" />
                    </div>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="x1" runat="server" />
        <asp:HiddenField ID="y1" runat="server" />
        <asp:HiddenField ID="x2" runat="server" />
        <asp:HiddenField ID="y2" runat="server" />
        <asp:HiddenField ID="Iwidth" runat="server" />
        <asp:HiddenField ID="Iheight" runat="server" />
        <div id="toolBar">
            <div id="toolBarPanel">
                <asp:Button ID="btnSaveImage" runat="server" Text="确定" OnClick="saveImg" Width="65px" />
                &nbsp;&nbsp;
                <input type="button" id="btnClose" title="取消" value="取消" style="width: 65px" />
            </div>
        </div>
    </form>
</body>
</html>
