﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="jRating.aspx.cs" Inherits="WebForm.jRating" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="Scripts/jRating.jquery.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.8.2.min.js"></script>
    <script src="Scripts/jRating.jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <!-- basic exemple -->
        <div class="exemple">
            <!-- in this exemple, 30 is the average and 1 is the id of the line to update in DB -->
            用户评价：
        <div class="basic" id="r" data-average="0"></div>
            <asp:HiddenField ID="rValue" Value="3" runat="server" />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {

            $(".basic").jRating({

                length: 5, // 星星的颗数   
                rateMax: 5, // 评分总分
                decimalLength: 0, // 评分的小数位，最大三位


                step: true, // 设置是否一颗一颗跳还是0.1课都可以
                type: 'big',// 可以选择small或者big，就是大星星还是小星星的区别而已
                smallStarsPath: '/Scripts/icons/small.png', // 设置小星星路径
                bigStarsPath: '/Scripts/icons/stars.png', // 设置大星星的路径，不然会在console中提示无法找到...图片
                showRateInfo: false,//设置是否显示鼠标移动到评分条上面的时候的分数

                canRateAgain: true,//是否可重复点击
                nbRates: 5,//可重复点击次数
                onShow: null ,
                onClick: function (element, rate) { //点击后修改服务器控件的值
                    alert(rate);
                    $("#rValue").val(rate);
                }
            });


            ////只允许评价一次， 再次评价时则提示  
            //$(".basic").live("click", function () {
            //    if ($(this).hasClass("jDisabled")) {
            //        if ($(this).attr("allowRate") == "1") {
            //            $(this).attr("allowRate", "0");
            //        } else {
            //            alert("今天已评价，不可再评价！");
            //        }
            //    }
            //});
        });
    </script>
</body>
</html>
