﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LayerShow.aspx.cs" Inherits="WebForm.LayerShow" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="Scripts/jquery-1.8.2.min.js"></script>

    <title>弹出的层</title>
   
</head>
<body>
    <form id="form1" runat="server">
        <div>
            这是一个弹出的子页面
     
        <asp:Button ID="Button1" runat="server" Text="保存" OnClick="Button1_Click" Width="71px" />
        </div>
        <div id="tt">close</div>
        <div id="t1">关闭窗口</div>

        
        <script type="text/javascript">
         
                function closeBox() {
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                }

            
            ////以下代码可用
            //$("#tt").click(function () {
            //    var index = parent.layer.getFrameIndex(window.name);
            //    parent.layer.close(index);
            //});
        </script>
    </form>
</body>
</html>
