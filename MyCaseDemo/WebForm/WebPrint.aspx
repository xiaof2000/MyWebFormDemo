﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebPrint.aspx.cs" Inherits="WebForm.WebPrint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script language="javascript">
        function preview() {
            bdhtml = window.document.body.innerHTML;
            sprnstr = "<!--startprint-->";
            eprnstr = "<!--endprint-->";
            prnhtml = bdhtml.substr(bdhtml.indexOf(sprnstr) + 17);
            prnhtml = prnhtml.substring(0, prnhtml.indexOf(eprnstr));
            window.document.body.innerHTML = prnhtml;
            window.print();
            //prnform.htext.value=prnhtml;
            //prnform.submit();
            //alert(prnhtml);
        }
</script>
</head>
<body>
    <form id="form1" runat="server">
    <h5>这不打印</h5>
        <p>如果不想打印页眉和页脚，可以在打印时设置，设置后以后就都不打印了（这个设置不影响word等的打印）</p>
        <!--startprint-->
        <div>
        <h3 style="text-align:center">这是标题</h3>
        <table style="width: 100%;">
            <tr>
                <td>标题</td>
                <td>名称</td>
                <td>内容</td>
            </tr>
            <tr>
                <td>1</td>
                <td>2</td>
                <td>3</td>
            </tr>
            <tr>
                <td>11</td>
                <td>22</td>
                <td>33</td>
            </tr>
            <tr>
                <td>标题标题</td>
                <td>名称名称</td>
                <td>内容内容</td>
            </tr>
        </table>
    
    </div>
    
    <p>
        <span style="float:left">经办人：张三&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 审核人：李四</span><span style="float:right">时间：2015-05-010</span> </p>

    <!--endprint-->
        <p></p>
        <h5>这也不打印</h5>
        <div align="center">
  <input type="button" name="print" value="预览并打印" onclick="preview()"/>
</div>
<style>
@media print {
   .Noprn {display:none;}
}
</style>
<p class="Noprn">不打印</p>
<table id=datagrid><tr><td>打印</td></tr></table>
<input class=Noprn type=button onclick="window.print()" value="print">
        <br />
        <br />
        <hr />
        <h3>解析后打印</h3>
        
        </form>
</body>
</html>
