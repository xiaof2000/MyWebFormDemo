﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="uploadifyDemo.aspx.cs" Inherits="WebForm.uploadifyDemo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>文件上传</title>
    <link href="Scripts/uploadify/uploadify.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.8.2.min.js"></script>
    <script src="Scripts/uploadify/jquery.uploadify.min.js"></script>
    <script src="Scripts/uploadify/jquery.uploadify.zh.js"></script>

    <script type="text/javascript">
        $(function () {
            //修改出错信息为中文
            function uploadify_onSelectError(file, errorCode, errorMsg) {
                var msgTxt = "上传失败\n";
                switch (errorCode) {
                    case -100: //SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED:
                        msgTxt += "每次最多上传 " + this.settings.uploadLimit + "个文件"; break;
                    case -110:
                        msgTxt += "文件大小超出系统限制的" + this.settings.fileSizeLimit; break;
                    default:
                        msgTxt += "错误代码：" + errorCode + "\n" + errorMsg;
                }
                alert(msgTxt);
                //errorMsg = msgTxt;
            };

            $("#uploadify").uploadify({
                //指定swf文件
                'swf': 'Scripts/uploadify/uploadify.swf',
                //后台处理的页面
                'uploader': 'UploadHandler.ashx',
                //按钮显示的文字
                'buttonText': '上传图片',
                //显示的高度和宽度，默认 height 30；width 120
                'height': 20,
                'width': 80,
                //上传文件的类型  默认为所有文件    'All Files'  ;  '*.*'
                //在浏览窗口底部的文件类型下拉菜单中显示的文本
                'fileTypeDesc': 'Image Files',
                //允许上传的文件后缀
                'fileTypeExts': '*.gif; *.jpg; *.png',
                'fileSizeLimit': '1MB',
                //发送给后台的其他参数通过formData指定
                //'formData': { 'someKey': 'someValue', 'someOtherKey': 1 },
                //上传文件页面中，你想要用来作为文件队列的元素的id, 默认为false  自动生成,  不带#
                //'queueID': 'fileQueue',
                //选择文件后自动上传
                'auto': true,
                //设置为true将允许多文件上传
                'multi': true,
                'removeCompleted': true,//	是否自动将已完成任务从队列中删除，如果设置为false则会一直保留此任务显示。
                //设置上传队列样式
                'itemTemplate': '<div id="${fileID}" class="uploadify-queue-item displayUpload">\
                    <div class="cancel">\
                        <a href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')">X</a>\
                    </div>\
                    <span class="fileName">${fileName} (${fileSize})</span><span class="data"></span>\
                </div>',
                'onFallback': function () {             //检测FLASH失败调用  
                    alert("您未安装FLASH控件，无法上传图片！请安装FLASH控件后再试。");
                },
                'overrideEvents': ['onDialogClose', 'onUploadSuccess', 'onUploadError', 'onSelectError'],
                'onSelect': uploadify_onSelect,
                'onSelectError': uploadify_onSelectError,
                'onUploadError': uploadify_onUploadError,
                'onUploadSuccess': function (file, data, response) {
                    alert('filename:' + file.name);

                }
            });
        });


    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <input type="file" name="uploadify" id="uploadify" />
            <%--用来作为文件队列区域--%>
            <div id="fileQueue">
            </div>
            
        </div>
        <div>
            <p>
                <a href="javascript:$('#uploadify').uploadify('upload')">上传</a>| 
            <a href="javascript:$('#uploadify').uploadify('cancel')">取消上传</a>

            </p>
        </div>
    </form>


</body>
</html>
