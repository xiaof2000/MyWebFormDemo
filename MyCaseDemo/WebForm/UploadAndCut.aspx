﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadAndCut.aspx.cs" Inherits="WebForm.UploadAndCut" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>图片上传并截图</title>
    <%--图片上传--%>
    <link href="Scripts/uploadify/uploadify.css" rel="stylesheet" />
    <script src="Scripts/uploadify/jquery.uploadify.min.js"></script>
    <%--截图用css,js--%>
    <link href="Scripts/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet" />

    <style type="text/css">
        #toolBar {
            height: 50px;
            width: 100%;
            position: fixed;
            bottom: 0;
            z-index: 900;
        }

        #toolBarPanel {
            float: right;
            width: 260px;
            line-height: 50px;
        }

        .image-box {
            min-width: 680px;
            min-height: 400px;
        }

        /*上传队列样式*/
        .displayUpload {
            display: block;
            top: 30%;
            left: 30%;
            position: absolute;
            width: 300px;
            z-index: 800;
        }
    </style>
    <script src="Scripts/jquery-1.8.2.js" type="text/javascript"></script>

    <script src="Scripts/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>
    <script src="Scripts/jcrop/js/jquery.Jcrop.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        
        function closeBox() {
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
        };

        //返回值
        function returnValue(returnControlId,filename)
        {
            var obj= window.parent.document.getElementById(returnControlId);
            if (obj!=null) {
                obj.value=filename;
                closeBox();
            }
        };


        $(function () {

            //修改出错信息为中文
            function uploadify_onSelectError(file, errorCode, errorMsg) {
                var msgTxt = "上传失败\n";
                switch (errorCode) {
                    case -100: //SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED:
                        msgTxt += "每次最多上传 " + this.settings.uploadLimit + "个文件"; break;
                    case -110:
                        msgTxt += "文件大小超出系统限制的" + this.settings.fileSizeLimit; break;
                    default:
                        msgTxt += "错误代码：" + errorCode + "\n" + errorMsg;
                }
                alert(msgTxt);
                //errorMsg = msgTxt;
            };

            $("#uploadify").uploadify({
                //指定swf文件
                'swf': 'Scripts/uploadify/uploadify.swf',
                //后台处理的页面
                'uploader': '../UploadHandler.ashx',
                //按钮显示的文字
                'buttonText': '上传图片',

                //显示的高度和宽度，默认 height 30；width 120
                //'height': 15,
                //'width': 80,
                //上传文件的类型  默认为所有文件    'All Files'  ;  '*.*'
                //在浏览窗口底部的文件类型下拉菜单中显示的文本
                'fileTypeDesc': 'Image Files',
                //允许上传的文件后缀
                'fileTypeExts': '*.gif; *.jpg; *.bmp; *.png',
                //发送给后台的其他参数通过formData指定
                'formData': { 'folder': $('#imgFilePath').val(),'privateFolder':$('#hfdPrivateFolder').val()},
                //上传文件页面中，你想要用来作为文件队列的元素的id, 默认为false  自动生成,  不带#
                //'queueID': 'fileQueue',
                //选择文件后自动上传
                'auto': true,
                //设置为true将允许多文件上传
                'multi': false,
                'removeCompleted': true,
                'removeTimeout':2,//上传队列的消失时间
                'fileSizeLimit': '1MB',
                //设置上传队列样式
                'itemTemplate': '<div id="${fileID}" class="uploadify-queue-item displayUpload">\
                    <div class="cancel">\
                        <a href="javascript:$(\'#${instanceID}\').uploadify(\'cancel\', \'${fileID}\')">X</a>\
                    </div>\
                    <span class="fileName">${fileName} (${fileSize})</span><span class="data"></span>\
                </div>',
                'onFallback':function(){             //检测FLASH失败调用  
                    alert("您未安装FLASH控件，无法上传图片！请安装FLASH控件后再试。"); 
                    window.open("http://www.adobe.com/go/getflashplayer");
                }, 
                'overrideEvents': ['onDialogClose', 'onSelectError'],//重写事件
                'onSelectError': uploadify_onSelectError,
                'onUploadSuccess': function (file, data, response) {
                    picName = file.name;
                    
                    alert('上传完成，原图片名称：' + file.name + "新文件名：" + data);
                    
                    $("#imgFileName").val(data);
                    showImage($('#imgFilePath').val(),$('#hfdPrivateFolder').val(),data);
                }
            });

            //重新显示一个要裁剪的图片
            function showImage(folder,privateFolder,picName) {
                jcrop_api.setImage( folder + privateFolder + picName);
            }


            //jQuery(function ($) {
            var jcrop_api,
                boundx,
                boundy,

                // Grab some information about the preview pane
                $preview = $('#image-box'),

               // xsize = $pcnt.width(),
                //ysize = $pcnt.height();
                xsize = <%= _sizeWidth %>,
                ysize = <%= _sizeHeight %>;


            //console.log('init', [xsize, ysize]);//输出宽高数值
            
            //$("#cutImage").click(function(){
            $('#target').Jcrop({
                //setSelect: [ 0, 0, 540, 330 ],//默认加上选择框
                boxWidth:680, //设置宽度
                boxHeight:460,
                onChange: updatePreview,
                onSelect: updatePreview,
                aspectRatio: xsize / ysize
            }, function () {
                // Use the API to get the real image size
                var bounds = this.getBounds();
                boundx = bounds[0];
                boundy = bounds[1];
                // Store the API in the jcrop_api variable
                jcrop_api = this;

                // Move the preview into the jcrop container for css positioning
                $preview.appendTo(jcrop_api.ui.holder);
            });
            //});

            function updatePreview(c) {
                if (parseInt(c.w) > 0) {
                    $('#x1').val(c.x);
                    $('#y1').val(c.y);
                    $('#x2').val(c.x2);
                    $('#y2').val(c.y2);
                    $('#Iwidth').val(c.w);
                    $('#Iheight').val(c.h);
                };
            };
        });        
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <input type="file" name="uploadify" id="uploadify" />

            <div style="padding-left: 200px; background-color: #808080; width: 400px; min-width: 400px">
                <%--用来作为文件队列区域--%>
                <div id="fileQueue">
                </div>
            </div>

        </div>
        <div class="container">
            <div class="image-box">
                <asp:Image ID="target" runat="server" alt="图片" />
            </div>
        </div>

        <asp:HiddenField ID="x1" runat="server" />
        <asp:HiddenField ID="y1" runat="server" />
        <asp:HiddenField ID="x2" runat="server" />
        <asp:HiddenField ID="y2" runat="server" />
        <asp:HiddenField ID="Iwidth" runat="server" />
        <asp:HiddenField ID="Iheight" runat="server" />
        <asp:HiddenField ID="imgFileName" runat="server" />
        <asp:HiddenField ID="hfdPrivateFolder" runat="server" />
        <asp:HiddenField ID="imgFilePath" runat="server" />
        

        <div id="toolBar">
            <div id="toolBarPanel">
                <asp:Button ID="btnSaveImage" runat="server" Text="确定" OnClick="saveImg" Width="65px" />
                &nbsp;&nbsp;
                <input id="btnClose" type="button" value="取消" style="width: 65px" onclick="closeBox();" />

            </div>
        </div>
    </form>
</body>
</html>
