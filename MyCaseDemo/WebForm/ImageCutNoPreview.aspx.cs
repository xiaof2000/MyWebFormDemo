﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Design;

namespace WebForm
{
    public partial class ImageCutNoPreview : System.Web.UI.Page
    {
        protected int _sizeWidth = 150;
        protected int _sizeHeight = 100;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Request.QueryString["newWidth"]) || string.IsNullOrWhiteSpace(Request.QueryString["newHeight"]))
            {
                _sizeHeight = 150;
                _sizeWidth = 100;
            }
            else
            {
                _sizeWidth = int.Parse(Request.QueryString["newWidth"]);
                _sizeHeight = int.Parse(Request.QueryString["newHeight"]);
            }


            string picName = string.Empty;
            if (string.IsNullOrWhiteSpace(Request.QueryString["picName"]))
            {
                picName = "jcropDemo.jpg";
            }
            else
            {
                picName = Request.QueryString["picName"];
            }
            
            string picPath =  "/UploadFiles/" + picName + "?" + DateTime.Now.Millisecond.ToString(); //加入随机数，用于强制刷新图片，以便于图片重新从服务器上获取
            target.ImageUrl = picPath;
           

        }

        protected void saveImg(object sender, EventArgs e)
        {
            if (IsPostBack)
            {

                string[] urls = target.ImageUrl.Split('/');
                string _url = urls.Last();

                string tempurl = Path.Combine(Server.MapPath("~/UploadFiles/"), _url);
                int startX =   0;
                int startY =   0;
                int width =    0;
                int height = 0;
                try
                {
                     startX = (int)double.Parse(x1.Value);
                     startY = (int)double.Parse(y1.Value);
                     width = (int)double.Parse(Iwidth.Value);
                     height = (int)double.Parse(Iheight.Value);
                }
                catch
                {
                    return;
                }

                ImgReduceCutOut(startX, startY, width, height, tempurl, tempurl);


                //注意：如果调用前台的js函数时，一定要将js函数放在html的head中或用其它办法保证要调用的方法已注册
                //关闭本窗口
                ClientScript.RegisterStartupScript(this.GetType(), "", "<script>closeBox();</script>");//调用前台的JS函数;

            }
        }


        /// <summary>
        /// 缩小裁剪图片
        /// </summary>
        /// <param name="int_Width">要缩小裁剪图片宽度</param>
        /// <param name="int_Height">要缩小裁剪图片长度</param>
        /// <param name="input_ImgUrl">要处理图片路径</param>
        /// <param name="out_ImgUrl">处理完毕图片路径</param>
        public void ImgReduceCutOut(int startX, int startY, int int_Width, int int_Height, string input_ImgUrl, string out_ImgUrl)
        {
            // ＝＝＝上传标准图大小＝＝＝
            int int_Standard_Width = 120;
            int int_Standard_Height = 120;

            int Reduce_Width = 0; // 缩小的宽度
            int Reduce_Height = 0; // 缩小的高度
            int CutOut_Width = 0; // 裁剪的宽度
            int CutOut_Height = 0; // 裁剪的高度
            int level = 100; //缩略图的质量 1-100的范围

            // ＝＝＝获得缩小，裁剪大小＝＝＝
            if (int_Standard_Height * int_Width / int_Standard_Width > int_Height)
            {
                Reduce_Width = int_Width;
                Reduce_Height = int_Standard_Height * int_Width / int_Standard_Width;
                CutOut_Width = int_Width;
                CutOut_Height = int_Height;
            }
            else if (int_Standard_Height * int_Width / int_Standard_Width < int_Height)
            {
                Reduce_Width = int_Standard_Width * int_Height / int_Standard_Height;
                Reduce_Height = int_Height;
                CutOut_Width = int_Width;
                CutOut_Height = int_Height;
            }
            else
            {
                Reduce_Width = int_Width;
                Reduce_Height = int_Height;
                CutOut_Width = int_Width;
                CutOut_Height = int_Height;
            }

            // ＝＝＝通过连接创建Image对象＝＝＝
            input_ImgUrl = input_ImgUrl.Substring(0, input_ImgUrl.IndexOf("?")); //去除随机数
            System.Drawing.Image oldimage = System.Drawing.Image.FromFile(input_ImgUrl);
            oldimage.Save(Server.MapPath("tepm.jpg"));
            oldimage.Dispose();

            //// ＝＝＝缩小图片＝＝＝
            //System.Drawing.Image thumbnailImage = oldimage.GetThumbnailImage(Reduce_Width, Reduce_Height, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);
            Bitmap bm = new Bitmap(Server.MapPath("tepm.jpg"));

            // ＝＝＝处理JPG质量的函数＝＝＝
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            ImageCodecInfo ici = null;
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.MimeType == "image/jpeg")
                {
                    ici = codec;
                    break;
                }
            }
            EncoderParameters ep = new EncoderParameters();
            ep.Param[0] = new EncoderParameter(Encoder.Quality, (long)level);

            // ＝＝＝裁剪图片＝＝＝
            Rectangle cloneRect = new Rectangle(startX, startY, CutOut_Width, CutOut_Height);
            PixelFormat format = bm.PixelFormat;
            Bitmap cloneBitmap = bm.Clone(cloneRect, format);

            // ＝＝＝保存图片＝＝＝
            out_ImgUrl = out_ImgUrl.Substring(0, out_ImgUrl.IndexOf("?")); //去除随机数
            cloneBitmap.Save(out_ImgUrl, ici, ep);
            bm.Dispose();
        }

        public bool ThumbnailCallback()
        {
            return false;
        }
    }
}